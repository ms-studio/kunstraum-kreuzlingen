<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */

get_header(); ?>

<!-- page-ausstellungen.php -->

<div id="main" role="main">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  
<?php endwhile; endif; ?>

<div <?php post_class('mainframe page-ausstellungen') ?> id="page-ausstellungen">


<?php include( TEMPLATEPATH . '/inc/pages-expo-nav.php' ); ?>
	
  <article>
  
  <div class="print-button-expo print-pdf"><a class="print-button small-font" href="#" onClick="window.print();return false">Drucken</a></div>
  
  <div class="full-block clearfix">
	    <div class="main-content clearfix">
	    
	    <?php 
	    query_posts(array(
	    	'posts_per_page' => -1, // show everything...
	    	'category' => array(3, 4), // Kunstraum + Tiefparterre
	    	'orderby' => 'post_date',
	    	'order' => 'DESC'
	    	)); 
	    p2p_type( 'posts_to_kuenstler' )->each_connected( $wp_query );
	    
	    
	    // Declare some helper vars
	    $previous_year = $year = 0;
	    $previous_month = $month = 0;
	    $ul_open = false;
	    
	    if (have_posts()) : while (have_posts()) : the_post(); ?>
	    
	    <?php
  		  	// Setup the post variables
  		  	setup_postdata($post);
  		  	
  		  	$current_post_id = get_the_ID();
  		   
  		  	$year = mysql2date('Y', $post->post_date);
  		  	$month = mysql2date('n', $post->post_date);
  		  	$day = mysql2date('j', $post->post_date);
  		  	  				 
  		  	?>
  		   
  		  	<?php if($year != $previous_year ) : ?>
  		   
  		  		<?php if($ul_open == true) : ?>
  		  		</ul></div>
  		  		<?php endif; ?>
  		   		
  		   		<div class="expos-year clearfix">
  		  		<h3 class="h3"><?php the_time('Y'); ?></h3>
  		  		
  		  		<ul class="ul clean">
  		   
  		  		<?php $ul_open = true; 
  		  		
  		  		 endif; 
  		  		$previous_year = $year; 
  		  		$previous_month = $month; 
  		  			
  		  	?>
  		   <li class="li-expo">
  		   <?php 
  		   if ( in_category( 'vorschau' ) ) {
  	?><div class="field-1 expo-vorschau"><span class="prefix small-font">vorschau</span><?php
  		   	// material_types
  		   } elseif ( in_category( 'aktuell' ) ) {
  		   	?><div class="field-1 expo-aktuell"><span class="prefix small-font">aktuell</span><?php
  		   } else {
  		   	echo('<div class="field-1">');
  		   }
  		   ?>
  		  	
  		  	<?php 
  		  	// check for meta fields
  		  		$kk_kuenstler = get_post_meta($post->ID, 'Künstler-Name', true);
  		  		$kk_short_date = get_post_meta($post->ID, 'Datum-kurz', true);
  		  	
  		  	 if($kk_kuenstler !== '') {
  		  	 		echo '<span class="small-font kuenstler">';
  		  			echo $kk_kuenstler;
  		  			echo '</span>';
  		  			//echo('done');
  		  			} else { 
  		  			
  		  		// Find connected pages
//  		  		p2p_list_posts_nolink( $post->connected, 'before_list=&after_list=&before_item=<div>Künstler: &after_item=</div>&hyperlinked=false' );
  		  		
  		  		p2p_list_posts_nolink( $post->connected, array(
  		  			'before_list' => '',
  		  			'after_list'  => '',
  		  			'before_item' => '<span class="small-font kuenstler">',
  		  			'after_item'  => '</span>',
  		  		) );
  		  		}
  		  	?>	
  		  	
  		  	<span class="small-font ital titel"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span> <?php 
  		  	
  		  			// The short DATE format
  		  			
  		  			if($kk_short_date !== '') {
	  		  			echo '<span class="small-font datum-kurz">';
	  		  			echo $kk_short_date;
	  		  			echo '</span>';
	  		  			
  		  			} ?>
  		  	</div><!-- .field-1 -->
  		  	
  		  	<div class="field-2">
  		  	
  		  	<?php //image_toolbox(); 
  		  	
  		  	// THINGS we NEED:
  		  	
  		  	// Presse 
  		  	// Publications
  		  	// Tribune de C.
  		  	?>
  		  	
  		  	<div class="small-font sub-items sub-itm-presse">
	  		  	<?php 
	  		  	$itemcounter = 0 ;
	  		  	$pressecounter = 0;
	  		  	
	  		  	// Find connected pages - PRESSE
	  		  	$connected_presse = new WP_Query( array(
	  		  	
	  		  	 'posts_per_page' => -1, // we need just one...
	  		  	   'connected_type' => 'materials_to_posts',
	  		  	  // 'nopaging' => true,
	  		  	'connected_items' => $current_post_id, // get_queried_object_id(), // $this_post_id
	  		  	  'post_type' => 'kk_material',
	  		  	  'post_status' => array ('publish', 'future'),	    
	  		  	  'orderby' => 'date',
	  		  	  'order' => 'DESC', // desc = newest first
	  		  	  'tax_query' => array(
  		  	  		array(
  		  	  			'taxonomy' => 'material_types',
  		  	  			'field' => 'slug',
  		  	  			'terms' => 'presse' )
	  		  	  ) ) );
	  		  	// Display connected pages
	  		  	
	  		  	if ( $connected_presse->have_posts() ) :
	  		  	while ( $connected_presse->have_posts() ) : $connected_presse->the_post(); 
	  		  	//echo "Presse (1)";
			  		  	$attachments = get_children(array('post_parent'=>$post->ID,'post_mime_type' => 'application/msword,application/pdf'));
			  		  	$nbAttch = count($attachments);
			  		  		if ( $nbAttch > 0 ) {
			  		  			// echo 'Presse ('.$nbAttch.')';
			  		  			// the_title();
			  		  			$pressecounter = ($pressecounter + $nbAttch);
			  		  		} 
			  		  	
			  		  	$itemcounter++;
	  		  	endwhile; 
	  		  	
	  		  	echo "Presse (". $pressecounter .")";
	  		  	// Prevent weirdness
	  		  	wp_reset_postdata();
	  		  	endif;
	  		  	// end PRESSE
	  		  	?>
  		  	</div>
  		  	
  		  	<div class="small-font sub-items sub-itm-publikationen">
  		  		<?php 
  		  		$itemcounter = 0 ;
  		  		// Find connected pages - PUBLIKATIONEN
  		  		$connected_presse = new WP_Query( array(
  		  		 'posts_per_page' => -1,
  		  		   'connected_type' => 'materials_to_posts',
  		  		  'nopaging' => true,
  		  		'connected_items' => $current_post_id, // get_queried_object_id(), // $this_post_id
  		  		  'post_type' => 'kk_material',
  		  		  'post_status' => array ('publish', 'future'),	    
  		  		  'orderby' => 'date',
  		  		  'order' => 'DESC', // desc = newest first
  		  		  'tax_query' => array(
  		  	  		array(
  		  	  			'taxonomy' => 'material_types',
  		  	  			'field' => 'slug',
  		  	  			'terms' => 'publikationen' )
  		  		  ) ) );
  		  		// Display connected pages
  		  		if ( $connected_presse->have_posts() ) :
  		  		while ( $connected_presse->have_posts() ) : $connected_presse->the_post(); 
  		  		//echo "Presse (1)";
  		  		$itemcounter++;
  		  		endwhile; 
  		  		echo "Publikationen (". $itemcounter .")";
  		  		// Prevent weirdness
  		  		wp_reset_postdata();
  		  		endif;
  		  		// end PUBLIKATIONEN
  		  		?>
  		  	</div>
  		  	
  		  	<div class="small-font sub-items sub-itm-tdc">
  		  		<?php 
  		  		$itemcounter = 0 ;
  		  		// Find connected pages - Tribune de Critique
  		  		$connected_presse = new WP_Query( array(
  		  		 'posts_per_page' => -1,
  		  		   'connected_type' => 'materials_to_posts',
  		  		  'nopaging' => true,
  		  		'connected_items' => $current_post_id, // get_queried_object_id(), // $this_post_id
  		  		  'post_type' => 'kk_material',
  		  		  'post_status' => array ('publish', 'future'),	    
  		  		  'orderby' => 'date',
  		  		  'order' => 'DESC', // desc = newest first
  		  		  'tax_query' => array(
  		  	  		array(
  		  	  			'taxonomy' => 'material_types',
  		  	  			'field' => 'slug',
  		  	  			'terms' => 'tribune-de-critique' )
  		  		  ) ) );
  		  		// Display connected pages
  		  		if ( $connected_presse->have_posts() ) :
  		  		while ( $connected_presse->have_posts() ) : $connected_presse->the_post(); 
  		  		//echo "Presse (1)";
  		  		$itemcounter++;
  		  		endwhile; 
  		  		echo "Tribune de Critique (". $itemcounter .")";
  		  		// Prevent weirdness
  		  		wp_reset_postdata();
  		  		endif;
  		  		// end Tribune de Critique
  		  		?>
  		  	</div>
  		  	
	  		<div class="small-font sub-items sub-itm-img">
	  		  	<?php //image_toolbox(); 
	  		  	$attachments = get_children(array('post_parent'=>$post->ID,'post_mime_type' => 'image'));
	  		  	$nbImg = count($attachments);
	  		  		if ( $nbImg > 0 ) {
	  		  		echo 'Bilder ('.$nbImg.')';
	  		  		}  ?>
	  		 </div>
	  		  	
	  		 <div class="small-font sub-items sub-itm-clip">	
	  		  	<?php 
	  		  		$video_expo = get_post_meta($post->ID, 'Video-expo-clip', true);
	  		  		$video_talk = get_post_meta($post->ID, 'Video-gespraech', true);
	  		  		if($video_expo !== '') { 
	  		  		
	  		  		echo 'Expo Clip (1)';
	  		  		} ?>
	  		  </div>
	  		  
	  		  <div class="small-font sub-items sub-itm-tlk">	
	  		  		<?php if($video_talk !== '' ) {
	  		  		echo 'Gespräche TV (1)';
	  		  		}
	  		  		?>
	  		  </div>
  		  		
  		  	</div><!-- .field-2 -->
  		  		  		  	
  		  	</li>
  		   
  		 <?php endwhile; endif; 
  		 	wp_reset_query(); ?>
  		 	
  		  	</ul>
  		  	</div>
  		    		 
  		</div><!--.main-content-->

   </div>
  
  </article>
   

</div>

<?php get_footer(); ?>
