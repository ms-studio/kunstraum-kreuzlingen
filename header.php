<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="de-DE"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="de-DE"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="de-DE"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="de-DE"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />
  
  <title><?php if(is_front_page()) {
  	echo 'Kunstraum Kreuzlingen'; 
  	} else {
  		if ( 'kk_material' == get_post_type() ) {
  			echo 'Material zu: '; 
  		}
			wp_title('&mdash;',true,'right'); 
			echo 'Kunstraum Kreuzlingen';
		}	
  	?> </title>
  <?php // ** DESCRIPTION v.0.2 **
  if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); 
  ?><meta name="description" content="<?php  
  	$descr = get_the_excerpt();
  	$text = preg_replace( '/\r\n/', ', ', trim($descr) ); 
  	// echo $text;
  	// need to improve this, return Roughly 155 Characters, of up to 265
  ?>" />
  <?php endwhile; endif; elseif(is_home()) : 
  ?><meta name="description" content="Der Kunstraum Kreuzlingen & Tiefparterre definieren sich als Ort für den «Discours» zeitgenössischer Kunst. Für das Tiefparterre werden experimentelle Medienprojekte und deren Forschungsbereich eigens entwickelt." />
  <?php endif; ?>
  	
  <meta name="author" content="Manuel Schmalstieg - Code" />
  
  <?php // ** SEO OPTIMIZATION v.0.2 **
  if ( is_attachment() || is_archive() || is_category() ) { 
  ?><meta name="robots" content="noindex,follow" /><?php } 
  else if(is_single() || is_page() || is_home()) { 
	?><meta name="robots" content="all,index,follow" /><?php 
	} 
	?>
	  
  <meta name="viewport" content="width=device-width,initial-scale=1" />
  
  <meta name="ICBM" content="47.651, 9.175" />
  <meta name="geo.country" content="CH" />
  <meta name="geo.region" content="CH-TG" />
  <meta name="geo.placename" content="Kreuzlingen" />
  <meta name="geo.position" content="47.651;9.175" />

  <style>.hidden {display: none;}</style>
  
	<?php wp_head(); ?>
	
  <!-- Wordpress Head Items -->
  <link rel="alternate" type="application/rss+xml" title="Kunstraum Kreuzlingen &raquo; Feed" href="<?php bloginfo('rss2_url'); ?>" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    
</head>

<body <?php 
if ( in_category( 'kunstraum' ) && is_single() ) {
	body_class('kunstraum no-js');
} elseif ( in_category( 'tiefparterre' ) && is_single() ) {
	body_class('tiefparterre no-js');
} else {
	body_class('no-js');
	}  ?>>
  
    <header id="top-header" role="banner" class="top-header">
    	<div class="zentr">
			<h1 class="h1 kk-logo unstyled-plus" title="<?php bloginfo('name'); ?>"><a href="<?php echo get_option('home'); ?>/"><img src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>images/logo-v8.png" alt="<?php bloginfo('name'); ?>" width="214" height="134" /></a></h1>
		
		<?php wp_nav_menu( array(
			 'theme_location' => 'main-menu',
			 'container'       => 'nav',
			 'container_class'       => 'clearfix main-menu default-menu horiz-list small-font',
			 'depth'           => 0,
			 //'link_after'       => '&nbsp;',
			 ) ); ?>
			 
			 
			<aside id="search-nav" class="aside search-nav">
			<div class="search-form" role="search"><?php get_search_form(); ?></div>    
			
			<div id="sortieren" class="sortieren">
			<h2 class="sortieren-h2"><a href="#" class="sortieren-nach">Sortieren nach</a></h2>
				<ul id="sortieren-ul" class="clean sortieren-ul hidden">
					<?php  
					// for Expo Clip
					// and Gespraeche TV
					// we need to count the occurrences of the Meta...
					
				$meta_query = new WP_Query( array(
						'meta_key' => 'Video-expo-clip',
						'posts_per_page' => -1
					)); 
				if ( $meta_query->have_posts() ) :
				  	// YES, we have something ...
				  	echo '<li class="li"><a href="/expo-clip/">Expo Clip</a>' ;
				  	$vid_clip_count = 0 ;
						  while( $meta_query->have_posts() ) : $meta_query->the_post();
						  // count for the number of posts
						  $vid_clip_count++;
						  endwhile; 
					 // then output it...
					 echo '&nbsp;<span class="itm-count">('. $vid_clip_count .')</span>' ;
					 echo '</li>';
				endif; 
				
				$meta_query = new WP_Query( array(
					'meta_key' => 'Video-gespraech',
					'posts_per_page' => -1
				)); 
				if ( $meta_query->have_posts() ) :
				  	// YES, we have something ...
				  	echo '<li class="li"><a href="/gespraeche-tv/">Gespräche TV</a>' ;
				  	$vid_clip_count = 0 ;
						  while( $meta_query->have_posts() ) : $meta_query->the_post();
						  // count for the number of posts
						  $vid_clip_count++;
						  endwhile; 
					 // then output it...
					 echo '&nbsp;<span class="itm-count">('. $vid_clip_count .')</span>' ;
					 echo '</li>';
				endif; 
				
					// for Synopsen, TdC, Reden etc
					// we need to make specific queries
					
					?>
					
					<li class="li"><a href="/synopsen/">Synopsen</a><?php 
					
					$args=array(
						'taxonomy' => 'material_types',
						'include' => '17', // 17 = synopsen
						// 15 = Expo clip
					  );
					  
					  $categories=get_categories($args);
					    foreach($categories as $category) { 
					    	echo '&nbsp;<span class="itm-count">('.$category->category_count.')</span>';
					    } 
					
					 ?></li>
					<li class="li"><a href="/tribune-de-critique/">Tribune de critique</a><?php 
					
					$args=array(
						'taxonomy' => 'material_types',
						'include' => '13', // 13 = tribune-de-critique
					  );
					  
					  $categories=get_categories($args);
					    foreach($categories as $category) { 
					    	echo '&nbsp;<span class="itm-count">('.$category->category_count.')</span>';
					    } 
					
					 ?></li>
					 
					 <li class="li"><a href="/reden/">Reden</a><?php 
					 
					 $args=array(
					 	'taxonomy' => 'material_types',
					 	'include' => '20', // 20 = reden
					   );
					   
					   $categories=get_categories($args);
					     foreach($categories as $category) { 
					     	echo '&nbsp;<span class="itm-count">('.$category->category_count.')</span>';
					     } 
					 
					  ?></li>
					 
					 <?php  
					 
					 $args=array(
					 	'taxonomy' => 'material_types',
					 	'include' => '5,11,14',
					 	// 'orderby' => 'menu_order', menu_order not accepted
					 	// 5 = Events
					 	// 11 = Presse
					 	// 14 = Publikationen
					   );
					   
					   $categories=get_categories($args);
					     foreach($categories as $category) { 
					     	echo '<li class="li"><a href="/'. $category->slug.'/">'. $category->name.'</a>';
					     	echo '&nbsp;<span class="itm-count">('.$category->category_count.')</span>';
					     	echo '</li>';
					     } 
					 
					 ?>
				</ul>
				
				<?php 
				
				// http://codex.wordpress.org/Template_Tags/wp_list_categories
				
				// http://codex.wordpress.org/Function_Reference/get_categories
								
				// Synopse: Tag ID 17
				
				// taxonomy: material_types
				// post_type: kk_material
				
				?>
			</div>
			
			<?php if ( is_home() ) {
			    ?> <a href="http://kunstgesellschaft-tg.ch/" target="_blank" class="unstyled-plus logo-kunstgesellschaft"><img src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>images/kunstgesellschaft.png" alt="Thurgauische Kunstgesellschaft" width="120" height="24" /></a>
			<?php } ?>
			
			</aside>
			
			<?php if ( is_home() ) {
			    ?>
			    
			    <?php    
			     $info_query = new WP_Query( array(
			     	'page_id' => 8,
			     	) ); 
			     
			     while( $info_query->have_posts() ) : $info_query->the_post();
			     ?>       
			     	<div id="infobox" class="infobox infobox-home darker-grey">
			        	<div class="news-list-item small-font infobox-home">
				        	<div class="header">
				        		<h2 class="h2 white bold">Kunstraum Kreuzlingen &amp;&nbsp;Tiefparterre</h2>
				        		
				        	</div>
				        	
				        	<div class="full-block clearfix content">
				            	<?php the_content('mehr Information'); ?>
				            </div>
			        	</div>
			   </div><!-- #infobox -->
			    <?php endwhile; 
			    //  wp_reset_postdata(); ?>  
			    
		<?php } else { ?>
			    <div id="infobox" class="infobox hidden darker-grey">
			    </div>
		<?php } ?>
						
		</div><!-- .zentr -->
		
    </header><!-- #top-header -->
    
    <div id="container" class="zentr">

