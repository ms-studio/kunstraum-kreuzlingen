<?php
/*
Template Name: Expo-Events
*/

//if($_SERVER['HTTP_X_REQUESTED_WITH']==''){
	// does not show when served via AJAX
	get_header(); 
//} 
?>


<!-- page-events.php -->

<div id="main" role="main" class="page-events">

<?php
$eventid = $_GET['eventid'];

// eventid is passed through the URL 

// if ($eventid !== '')

if (isset($eventid)) { 

	  query_posts( 'p=' . $eventid . '' );
	  
	  
	  
	  if (have_posts()) : while (have_posts()) : the_post(); 
	  ?>
	  
	  <article id="mainframe" <?php post_class('events-page mainframe single-article') ?> data-postid="events-page">
	  	 <div class="mainframe-content small-font">
	  	 <div class="main-content medium-font compact">
	  	 
	    
	    <?php // Find connected EVENTS 
	    $connected = new WP_Query( array(
	      'posts_per_page' => -1,
	      'connected_type' => 'materials_to_posts',
	      //'category_name' => 'events', ??
	      // taxonomy: material_types
	      'tax_query' => array(
	      		array(
	      			'taxonomy' => 'material_types',
	      			'field' => 'slug',
	      			'terms' => 'events'
	      		)
	      	),
	      'connected_items' => get_queried_object_id(),
	      'post_type' => 'kk_material',
	      'post_status' => array ('publish', 'future'), //important
	      'orderby' => 'date',
	      'order' => 'ASC',
	    ) );
	    
	    // Display connected pages
	    if ( $connected->have_posts() ) :
	    ?>
	  	    <ul class="clean atom-list">
	  	    <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
	  	    	<li><h3 class="bold-title"><?php the_title(); ?></h3>
	  	    		<div class="compact-content"><?php the_content(); ?></div>
	  	    		<?php edit_post_link('bearbeiten', '<p class="small">[ ', ' ]</p>'); ?>
	  	    	</li>
	  	    <?php endwhile; ?>
	  	    </ul> <br/>
	  	    <?php // Prevent weirdness
	  	    wp_reset_postdata();
	  	    else : ?>
	  	    <!-- keine Materialien -->
	    <?php 
	    endif; //connected Events
	    ?>
	    
	  		</div>
	  		</div>
	  	</article>
	  	
	  	  
	  <?php endwhile; endif; ?>
	  
	  <?php
	  
}

else {

	
	if (have_posts()) : while (have_posts()) : the_post(); 
	?>
	
	<article id="mainframe" <?php post_class('events-page mainframe single-article') ?> data-postid="events-page">
		 <div class="mainframe-content">
		 
		 <header class="mainframe-content mainframe-header">
		   <h1 class="main-title h1"><?php the_title(); ?></h1>
		 </header>
		 
		 <div class="main-content medium-font">
		 	<?php the_content('Read the rest of this entry &raquo;'); ?>
		 </div>
		 		 
 		</div>
 	</article>
		 	
		 <?php endwhile; endif; ?>
	
	<?php

}
  // 
 
  ?>
  
</div>

<?php 
//if($_SERVER['HTTP_X_REQUESTED_WITH']==''){
	// does not show when served via AJAX
	get_footer(); 
//} 
?>
