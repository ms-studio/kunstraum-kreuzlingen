## Font Sizes

Font-size is defined as:

default font-size: 10 px

10px 'Open Sans', Arial, Helvetica, Sans-Serif

### Other defined sizes:

.h2 = 170% -> 17pt

.font-small, .small-font = 110% -> 11pt

.larger-font = 145% -> 14.5pt

.exhib-block-front = 180% -> 18pt

.infobox-home .h2 = 150% -> 15pt

.listing-main-title = 190% -> 19pt

***

### Changes of March 2016

Move small font size from 10/11pt to 14pt.

- changed body from 10pt to 0.875em (= 14pt)
- .h2 = change to 125% -> 18pt
- .exhib-block-front -> 140%
- .infobox-home .h2 -> 130%
- .larger-font : change to 110%

***