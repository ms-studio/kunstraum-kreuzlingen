## Image Sizes

Aktuelle grösse auf austellungs-seiten:
klein-format (.expo-img-medium): 244 x 183 px
mittel (.expo-img-large): 496 x 374 px
gross: 748

1st image format:

244+244+8(margin) = 496 pixels wide

183+183+8 = 374 pixels high

# Images Defined:

add_image_size( 'front-vertical', 222, 252, true ); // cropped
= not used anymore! disable

add_image_size( 'landscape-medium', 244, 183, true ); // cropped
-> new : 366 x 274.5 -> 488 x 366
add_image_size( 'landscape-large', 496, 374, true ); // cropped
-> new: 744 x 561



settings under Media:
thumbnail: 150 x 150
medium: 300 x 300
large: 1024 x 1024


increase those sizes!
