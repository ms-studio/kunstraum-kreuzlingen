<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */

get_header(); ?>

  <div id="main" role="main">
  
  <div <?php post_class('mainframe') ?>>

  <?php if (have_posts()) : ?>

    <h1 class="h1">Gefundene Seiten für: <?php printf( get_search_query() ); ?></h1>


    <?php while (have_posts()) : the_post(); ?>

      <article <?php post_class('search-item') ?>>
        <h3 class="medium-font" id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
        
      </article>

    <?php endwhile; ?>

    <nav>
      <div><?php next_posts_link('&laquo; Ältere Einträge') ?></div>
      <div><?php previous_posts_link('Neuere Einträge &raquo;') ?></div>
    </nav>

  <?php else : ?>

    <h1 class="ultra-gross">Nichts gefunden</h1>

  <?php endif; ?>

  </div>
  </div>

<?php get_footer(); ?>
