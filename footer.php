<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */
?>

  <footer id="main-footer" class="main-footer centr-menu-container" role="contentinfo">
  
  	<ul id="credits" class="credits default-menu footer-menu horiz-list">
  	<li><p>Videos, Bilder, Texte © Kunstraum und Autoren, <?php echo date('Y'); ?></p></li>	
  	</ul>
  
  </footer>
</div> <!--! end of #container -->

  <?php wp_footer(); ?>
  
<script>
       
jQuery(document).ready(function($){	
       				
// Produce some page-specific CSS styles
	
	$('#archive-box .year-archive-<?php the_time('Y') ?>').addClass('active-year');
	$('#archive-box .<?php $titleslug = basename(get_permalink()); 
	echo $titleslug;?>').addClass('active-page');
	// hide other years on hover
	$('#archive-box .year-archive').hover(
		function () { // mouse-in
				// $('#archive-box .ul-year-archive').hide();
		    // $(this).children(".ul-year-archive").show();
		  }, 
		  function () { // mouse-out
		    // $('#archive-box .ul-year-archive').hide();
		    // $('#archive-box .year-archive-<?php the_time('Y') ?> .ul-year-archive').show();
		  }
		); // end hover
       				
}); // end document ready
       		       		 
 </script>

</body>
</html>
