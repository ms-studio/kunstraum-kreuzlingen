<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */

get_header(); 

/**
 * The structure of this page: 
 *
 * 1.     Aktuell + Vorschau
 *   1.1  	Section: Kunstraum + Tiefparterre aktuell
 *   1.2  	Section: Vorschau
 * 2.     Events + News
 *   2.1  	Section: Events
 *   2.2	Section: News
 *
 */

?>

<div id="main" role="main" class="mainframe">

<section id="current-exhibits" class="current-exhibits half-sct half-sct-1st">
<h1 class="hidden">Aktuell und Vorschau</h1>

	<section id="aktuell">
	
	<h2 class="mini-capitals">Aktuell</h2>
	
	  <?php 
	  global $wp_query;
	  $args = array_merge( $wp_query->query, array( 
	  'posts_per_page' => -1,
	  'category__and' => array(3, 9), // 3 = Kunstraum, 9 = aktuell
	   ) );
	  query_posts( $args );
	  
	  p2p_type( 'posts_to_kuenstler' )->each_connected( $wp_query );
	  
	   if (have_posts()) : while (have_posts()) : the_post(); ?>
	    
	      <article <?php post_class('article exhib-block-front exhib-block-2nd home-kunstraum') ?>>
	      	<a href="<?php the_permalink() ?>" rel="bookmark" class="unstyled blok">
	
	      	<h2 class="h2">
	      	<?php 
	      	// check for meta fields
	      		$kk_kuenstler = get_post_meta($post->ID, 'Künstler-Name', true);
	      	
	      	 if($kk_kuenstler !== '') {
	      			echo $kk_kuenstler;
	      			//echo('done');
	      			} else { 
	      		// Find connected pages - Kuenstler
	      	  // p2p_list_posts( $post->connected ); // produces UL > LI
	      	  p2p_list_posts_nolink( $post->connected, 'before_list=&after_list=&before_item= &after_item=&hyperlinked=false' );
	      	  }
	      	  ?></h2>
	      	<h2 class="h2 ital"><span class="u-line-bold"><?php the_title(); ?></span></h2>
	      	
	      	<?php // check for meta fields : Title, Year, Type ...
	      			$kk_date = get_post_meta($post->ID, 'Datum', true);
	      			if($kk_date !== '') {
	      				echo '<p class="datum small-font">';
	      				echo $kk_date;
	      				echo '</p>';
	      				} 
	      	?>
	      	
	          </a>      
	      </article>      
	 
	    <?php endwhile; endif; // wp_reset_postdata(); 
	    	wp_reset_query();  // Ends main loop
	     ?>
	     
		<?php 
		query_posts(array( /////// Now: TIEFPARTERRE
			'posts_per_page' => -1,
			'category__and' => array(4, 9), // 4 = Tiefparterre, 9 = aktuell
			)); 
		p2p_type( 'posts_to_kuenstler' )->each_connected( $wp_query );
		if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<article <?php post_class('article exhib-block-front exhib-block-1st home-tiefparterre tiefparterre-style') ?>>
			<a href="<?php the_permalink() ?>" rel="bookmark" class="unstyled blok">
			<h2 class="h2">
				<?php 
				// check for meta fields
					$kk_kuenstler = get_post_meta($post->ID, 'Künstler-Name', true);
				
				 if($kk_kuenstler !== '') {
						echo $kk_kuenstler;
						// echo "done";
						} else { 
				// Find connected pages - Kuenstler
				// https://github.com/scribu/wp-posts-to-posts/wiki/Using-p2p_list_posts%28%29
	 			 p2p_list_posts_nolink( $post->connected, 'before_list=&after_list=&before_item= &after_item=&hyperlinked=false' );
	 			 } // end if
	 			 
	 			 ?></h2>
	 			 <h2 class="h2 ital"><span class="u-line-bold"><?php the_title(); ?></span></h2>
	 			 
	 			 <?php // check for meta fields : Title, Year, Type ...
	 			 		$kk_date = get_post_meta($post->ID, 'Datum', true);
	 			 		if($kk_date !== '') {
	 			 			echo '<p class="datum small-font">';
	 			 			echo $kk_date;
	 			 			echo '</p>';
	 			 		} ?>
	 			 
	 			     </a>      
	 			 </article>
	 			 
		<?php endwhile; endif; 
			wp_reset_query(); ?>
			
		</section>
		<section id="vorschau" class="section-vorschau">
		
		<h2 class="mini-capitals">Vorschau</h2>
		
		<?php 
		
		$counter = 0 ;
		
		query_posts(array( /////// Now: VORSCHAU
			'posts_per_page' => -1,
			'category__and' => array(8), // 8 = Vorschau
			'orderby' => 'post_date',
			'order' => 'ASC'
			)); 
		p2p_type( 'posts_to_kuenstler' )->each_connected( $wp_query );
		if (have_posts()) : while (have_posts()) : the_post(); 
		
		$counter++; // increment by one
		
			if ( $counter & 1 ) {
			  //odd
			  $evenodd = "odd";
			} else {
			  //even
			  $evenodd = "even";
			}
		
		?>
		
		<article <?php post_class('article news-list-item exhib-block-vorschau '. $evenodd ) ?>>
			<a href="<?php the_permalink() ?>" rel="bookmark" class="unstyled blok">
			<h2 class="h2">
				<?php 
				// check for meta fields
					$kk_kuenstler = get_post_meta($post->ID, 'Künstler-Name', true);
				
				 if($kk_kuenstler !== '') {
						echo $kk_kuenstler;
						// echo "done";
						} else { 
					 p2p_list_posts_nolink( $post->connected, 'before_list=&after_list=&before_item= &after_item=&hyperlinked=false' );
					 } // end if
					 
					 ?></h2>
					 <h2 class="h2 ital"><span class="u-line-thin"><?php the_title(); ?></span></h2>
					 
					 <?php // check for meta fields : Title, Year, Type ...
					 		$kk_date = get_post_meta($post->ID, 'Datum', true);
					 		if($kk_date !== '') {
					 			echo '<p class="datum small-font">';
					 			echo $kk_date;
					 			echo '</p>';
					 		} ?>
					 
					     </a>      
					 </article>
					 
		<?php endwhile; endif; 
			wp_reset_query(); ?>
		
		</section>
</section>
    
<section id="news-and-events" class="half-sct half-sct-2nd">

 <h1 class="hidden">Events und News</h1>
 
 	<section <?php post_class('news-list-v section-events') ?>>
 	<h2 class="mini-capitals">Events</h2>
   <?php    
    $event_query = new WP_Query( array(
    	'posts_per_page' => -1,
    	'post_type' => 'kk_material',
    	'post_status' => array ('publish'), // ('publish', 'future'),
    	'tax_query' => array(
    			array(
    				'taxonomy' => 'material_types',
    				'field' => 'slug',
    				'terms' => array('events','aktuell'),
    				'operator' => 'AND', 
    				// Possible operators are 'IN', 'NOT IN', 'AND'. 
    			)
    		),
    	'orderby' => 'date',
    	'order' => 'ASC',
    	) ); 
    	         
    while( $event_query->have_posts() ) : $event_query->the_post();
    ?>       
       	<article class="news-list-item small-font">
	       	<h3 class="bold"><?php the_title(); ?></h3>
	       	<?php the_content('mehr Information'); ?>  
	       	
	       	<?php edit_post_link('bearbeiten', '<p class="edit">[ ', ' ]</p>'); ?>
	       	   
       	</article>
           <?php endwhile; 
            wp_reset_postdata(); ?>    
    </section>    
    
    <section <?php post_class('news-list-v section-news') ?>>
    	<h2 class="mini-capitals">News</h2>
    <?php    
     $second_query = new WP_Query( array(
     	'posts_per_page' => -1,
     	'post_type' => 'kk_material',
     	'post_status' => array ('publish'), 
     	'tax_query' => array(
     			array(
     				'taxonomy' => 'material_types',
     				'field' => 'slug',
     				'terms' => array('news','aktuell'),
     				'operator' => 'AND', 
     				// Possible operators are 'IN', 'NOT IN', 'AND'. 
     			)
     		),
     	'orderby' => 'date',
     	'order' => 'ASC',
     	) ); 
     
     while( $second_query->have_posts() ) : $second_query->the_post();
     ?>       
        	<article class="news-list-item small-font news-green">
        	<?php // check for meta fields : Title, Year, Type ...
        			$kk_date = get_post_meta($post->ID, 'Datum', true);
        			if($kk_date !== '') {
        				echo '<div class="datum">';
        				echo $kk_date;
        				echo '</div>';
        				} ?>
        	
	        	<h3 class="bold"><?php the_title(); ?></h3>
	        	<?php the_content('mehr Information'); ?>
	        	
	        	 <?php edit_post_link('bearbeiten', '<p class="edit">[ ', ' ]</p>'); ?>
	        	 
        	</article>
            <?php endwhile; 
             wp_reset_postdata(); ?>    
     </section>   
     
  </section><!-- #news-and-events -->
       
</div>	<!-- #mainframe -->

<?php // get_sidebar(); ?>

<?php get_footer(); ?>


