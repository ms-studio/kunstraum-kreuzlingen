<?php
/*
Template Name: Künstler-AZ
*/

// get_header(); 

 ?>


<!-- page-kuenstlerAZ.php -->

<div id="main" role="main" class="page-events">

<article id="mainframe" <?php post_class('events-page mainframe single-article') ?> data-postid="events-page">
	 <div class="mainframe-content small-font">
	 <div id="kuenstler-az" class="">
	 
	 <h1 class="menu-title"><span class="border-bottom">Künstler A - Z</span></h1>
	 
	 <ul id="ul-kuenstler" class="clean atom-list unstyled hoverable">

<?php	
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'kuenstler',
			'orderby' => 'title',
			'order'    => 'ASC',
		);

		query_posts( $args );
		
		// What we do here: just display a list of all existing ARTISTS
		
		// p2p_type( 'posts_to_kuenstler' )->each_connected( $wp_query );
		// p2p_type is not flexible enough for what we want to do...
	  
	  if (have_posts()) : while (have_posts()) : the_post(); 
	  
	  // OBJECTIVE : get the connected EXHIBITIONS
	  
	  // (because we need their URL)
	  
	  // issue #1 : some artists may not have an EXHIBITION attached.
	  
	  	$this_post_id = get_the_ID();
	  	
	  	// echo $this_post_id;
	  	
	  	$kk1_artist_name = get_the_title();
	  	
	  	// echo $kk1_artist_name;
	  
	  // NOW we FIRST check if any EXHIBITION is attached...
	  
			  $connected_artist = new WP_Query( array(
			  	'posts_per_page' => 1, // we need only one link (the newest)
			    'connected_type' => 'posts_to_kuenstler',
			    'connected_items' => $this_post_id,
			    //'post_type' => 'kuenstler',
			  ) );
			  
			  if ( $connected_artist->have_posts() ) :
			  
			  while ( $connected_artist->have_posts() ) : $connected_artist->the_post(); 

			  // 
			  $kk1_expo_url = get_permalink(); 
			  
			  // now the output...
			  ?>
			  
			  <li class="li-kuenstler">
			    <a class="a" href="<?php 
			    	echo $kk1_expo_url;
			  	?>"><?php 
			  		echo $kk1_artist_name; 
			  	?></a>
			  </li>
			  <?php
			  
			  endwhile; 
			  // wp_reset_postdata();
			  
			  else :
			  
			  // reset artist name to none ...
			  // just for safety ...
			  
			  $kk1_artist_name = '';
			  $kk1_expo_url = '#';
			 
			  endif; // end connected EXPO loop
			  
			  
			  endwhile; endif; 
			  // end KUENSTLER loop
			  ?>
	  
	  </div>
	  	</div>
	  </article>
	  
	  <?php
	  

  // 
 
  ?>
  
</div>

<?php 
 //	get_footer(); 
?>
