<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */
// if($_SERVER['HTTP_X_REQUESTED_WITH']==''){

// source: http://coding.smashingmagazine.com/?p=25742
// does not show when loaded with AJAX
	get_header(); 
// } 
?>

<!-- Template: single.php -->


<div id="main" role="main">

<?php // modify the main loop
//		global $wp_query;
//		$args = array_merge( $wp_query->query, array( 
//		'post_type' => array('post', 'material'),
//		'post_status' => array ('publish', 'future'), //important
//		 ) );
//		query_posts( $args );

// http://codex.wordpress.org/Function_Reference/query_posts

	 if (have_posts()) : while (have_posts()) : the_post(); ?>



<?php 
		if ( in_category( array( 'kunstraum', 'tiefparterre' ) )) {
			 include( TEMPLATEPATH . '/inc/single-expo.php' );
		}
		elseif ( 'kuenstler' == get_post_type() ) {
				include( TEMPLATEPATH . '/inc/single-kuenstler.php' );
		}
		elseif ( 'kk_material' == get_post_type() ) {
				include( TEMPLATEPATH . '/inc/single-material.php' );
		}
		else { 
				include( TEMPLATEPATH . '/inc/single-article.php' );
		}
 ?> 
	


<?php endwhile; else: ?>

  <p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>


</div><!-- #main -->

<?php 
// if($_SERVER['HTTP_X_REQUESTED_WITH']==''){
	get_footer(); 
// } 
?>

