<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */

require_once( 'functions/init.php' );

require_once( 'functions/cpt.php' );

require_once( 'functions/kuenstler.php' );


/* Disable the Admin Bar. */
// add_filter( 'show_admin_bar', '__return_false' );

function new_excerpt_more($more) {
	return '... <span class="a-link">mehr</span>';
}
add_filter('excerpt_more', 'new_excerpt_more');

function custom_excerpt_length( $length ) {
	return 28; // = number of words, was 35
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
        // set_post_thumbnail_size( 150, 150 ); // default Post Thumbnail dimensions   
}

if ( function_exists( 'add_image_size' ) ) { 
	//add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
	//add_image_size( 'landscape', 304, 184, true ); // true = cropped
	// add_image_size( 'front-vertical', 222, 252, true ); // cropped
	add_image_size( 'landscape-medium', 488, 366, true ); // cropped
	add_image_size( 'landscape-large', 744, 561, true ); // cropped
}

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
			array(
				'main-menu' => __( 'Hauptmenü' ),
				'info-menu' => __( 'Info-menü' ),
				//'footer-fabiana' => __( 'Pied de page Fabiana' ),
				//'footer-michel' => __( 'Pied de page Michel' )
				)
			);
}

// display future post content...
// source : http://wordpress.org/extend/plugins/the-future-is-now

function setup_future_hook() {
 // Replace native future_post function with replacement
 remove_action('future_post', '_future_post_hook');
 add_action('future_post', 'publish_future_post_now');
 // needed for every custom post-type !!!
 remove_action('future_kk_material','_future_post_hook');
  add_action('future_kk_material','publish_future_post_now');
}

function publish_future_post_now($id) {
 // Set new post's post_status to "publish" rather than "future."
 wp_publish_post($id);
}

add_action('init', 'setup_future_hook');


// post_2_posts plugin
// https://github.com/scribu/wp-posts-to-posts/wiki/Basic-usage

function my_connection_types() {
	// Make sure the Posts 2 Posts plugin is active.
	if ( !function_exists( 'p2p_register_connection_type' ) )
		return;

	p2p_register_connection_type( array(
		'name' => 'posts_to_posts',
		'from' => 'post',
		'to' => 'post',
		'reciprocal' => true,
		'title' => 'Kunstraum / Tiefparterre',
		// 'admin_box' => 'from',
	) );
	
	p2p_register_connection_type( array(
		'name' => 'posts_to_kuenstler',
		'from' => 'post',
		'to' => 'kuenstler',
		'reciprocal' => true,
		'title' => 'Ausstellung / Künstler',
		// 'admin_box' => 'from',
	) );
	
	p2p_register_connection_type( array(
		'name' => 'materials_to_posts',
		'from' => 'kk_material',
		'to' => 'post',
		'reciprocal' => true,
		'title' => 'Ausstellung / Materialien',
		// 'admin_box' => 'from',
	) );
}
add_action( 'init', 'my_connection_types', 100 );



function p2p_list_posts_nolink( $posts, $args = array() ) {
	if ( is_object( $posts ) )
		$posts = $posts->posts;

	$args = wp_parse_args( $args, array(
		'before_list' => '<ul>', 'after_list' => '</ul>',
		'before_item' => '<li>', 'after_item' => '</li>',
		'template' => false
	) );

	extract( $args, EXTR_SKIP );

	if ( empty( $posts ) )
		return;

	echo $before_list;

	foreach ( $posts as $post ) {
		$GLOBALS['post'] = $post;

		setup_postdata( $post );

		echo $before_item;

		if ( $template )
			locate_template( $template, true, false );
		else
			//echo html( 'a', array( 'href' => get_permalink( $post->ID ) ), get_the_title( $post->ID ) );
			
			echo( get_the_title( $post->ID ) );
		echo $after_item;
	}

	echo $after_list;

	wp_reset_postdata();
}



// test for custom taxonomy
function has_custom_type( $type, $_post = null ) {
	if ( empty( $type ) )
		return false;
	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];
	if ( !$_post )
		return false;
	$r = is_object_in_term( $_post->ID, 'material_types', $type );
	if ( is_wp_error( $r ) )
		return false;
	return $r;
}


// custom taxonomies
// source: http://net.tutsplus.com/?p=11658
// et http://codex.wordpress.org/Function_Reference/register_taxonomy


// Renaming "Artikel" into "Ausstellungen":

// http://new2wp.com/snippet/change-wordpress-posts-post-type-news/
// ca December 14, 2010
// also: 
// http://wordpress.stackexchange.com/questions/9211/changing-admin-menu-labels

function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Ausstellungen';
	$submenu['edit.php'][5][0] = 'Alle Ausstellungen'; // Alle Artikel
	//$submenu['edit.php'][10][0] = 'Add News'; // Erstellen
	//$submenu['edit.php'][16][0] = 'News Tags'; // Schlagworte
	echo '';
}
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Ausstellungen';
	$labels->singular_name = 'Ausstellung';
	$labels->add_new = 'Erstellen';
	$labels->add_new_item = 'Erstellen';
	$labels->edit_item = 'Bearbeiten';
	$labels->new_item = 'Ausstellung';
	$labels->view_item = 'Ausstellung ansehen';
	$labels->search_items = 'Ausstellungen suchen'; // Artikel suchen
	$labels->not_found = 'Kein Resultat';
	$labels->not_found_in_trash = 'Kein Resultat'; // Keine Artikel im Papierkorb gefunden.
	$labels->name_admin_bar = 'Ausstellung'; // works !!!
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );


// Add custom taxonomies and custom post types counts to dashboard
// 
// http://codex.wordpress.org/Plugin_API/Action_Reference/right_now_content_table_end

add_action( 'right_now_content_table_end', 'my_add_counts_to_dashboard' );

function my_add_counts_to_dashboard() {

    // Custom post types counts
    $post_types = get_post_types( array( '_builtin' => false ), 'objects' );
    foreach ( $post_types as $post_type ) {
        $num_posts = wp_count_posts( $post_type->name );
        $num = number_format_i18n( $num_posts->publish );
        $text = _n( $post_type->labels->singular_name, $post_type->labels->name, $num_posts->publish );
        if ( current_user_can( 'edit_posts' ) ) {
            $num = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . '</a>';
            $text = '<a href="edit.php?post_type=' . $post_type->name . '">' . $text . '</a>';
        }
        echo '<td class="first b b-' . $post_type->name . 's">' . $num . '</td>';
        echo '<td class="t ' . $post_type->name . 's">' . $text . '</td>';
        echo '</tr>';

        if ( $num_posts->pending > 0 ) {
            $num = number_format_i18n( $num_posts->pending );
            $text = _n( $post_type->labels->singular_name . ' pending', $post_type->labels->name . ' pending', $num_posts->pending );
            if ( current_user_can( 'edit_posts' ) ) {
                $num = '<a href="edit.php?post_status=pending&post_type=' . $post_type->name . '">' . $num . '</a>';
                $text = '<a href="edit.php?post_status=pending&post_type=' . $post_type->name . '">' . $text . '</a>';
            }
            echo '<td class="first b b-' . $post_type->name . 's">' . $num . '</td>';
            echo '<td class="t ' . $post_type->name . 's">' . $text . '</td>';
            echo '</tr>';
        }
    }
}

// end Dashboard widget customization
////////////



// IMAGES

// (thumbnail, medium, large or full)

/**
 * Retrieve URL for large img.
 * copied from wp-includes/post.php - wp_get_attachment_thumb_url
 */
function kk_get_img_large_url( $post_id = 0 ) {
	$post_id = (int) $post_id;
	if ( !$post = get_post( $post_id ) )
		return false;
	if ( !$url = wp_get_attachment_url( $post->ID ) )
		return false;
	$sized = image_downsize( $post_id, 'large' );
	if ( $sized )
		return $sized[0];
	if ( !$thumb = wp_get_attachment_thumb_file( $post->ID ) )
		return false;
	$url = str_replace(basename($url), basename($thumb), $url);
	return apply_filters( 'kk_get_img_large_url', $url, $post->ID );
}

function first_img_toolbox($size = 'thumbnail', $howmany = 1) {
	if ( $images = get_children ( array (
		'post_parent'    => get_the_ID(),
		'post_type'      => 'attachment',
		'numberposts'    => $howmany , // -1 = show all
		'post_status'    => null,
		'post_mime_type' => 'image',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		// 'linked' => $link, // link = create link
	))) {
		foreach ( $images as $image ) {
			$attimg   = wp_get_attachment_image($image->ID,'landscape-large');
			//$atturl   = wp_get_attachment_url($image->ID);
			$largurl   = kk_get_img_large_url($image->ID);
			$attlink  = get_attachment_link($image->ID);
			//$postlink = get_permalink($image->post_parent);
			//$atttitle = apply_filters('the_title',$image->post_title);

			echo '<div class="expo-img-large img-fx-first"><a href="'.$largurl.'" class="expo-img-container">';
			echo $attimg;
			echo '</a></div>';
		}
	}
}

function expo_img_toolbox($offset = 0) {
	$imgcounter = 0 ;
	if ( $images = get_children ( array (
		'post_parent'    => get_the_ID(),
		'post_type'      => 'attachment',
		'numberposts'    => 99 , // -1 = show all _> offset not working
		'post_status'    => null,
		'offset' => $offset,
		'post_mime_type' => 'image',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		// 'linked' => $link, // link = create link
	))) {
		foreach ( $images as $image ) {
			$attimg   = wp_get_attachment_image($image->ID,'landscape-medium');
			//$atturl   = wp_get_attachment_url($image->ID);
			$largurl   = kk_get_img_large_url($image->ID);
			$attlink  = get_attachment_link($image->ID);
			$atttitle = apply_filters('the_title',$image->post_title);
			$imgcounter++;

			echo '<div class="expo-img-medium img-fx-medium" id="img-'.$imgcounter.'"><a href="'.$largurl.'" class="expo-img-container">';
			echo $attimg;
			?></a></div>
			<?php
		} // end foreach
	} // end if
}

function image_toolbox($size = 'thumbnail', $howmany = 1, $link = 'no', $list = 'no') {
	if ( $images = get_children ( array (
		'post_parent'    => get_the_ID(),
		'post_type'      => 'attachment',
		'numberposts'    => $howmany , // -1 = show all
		'post_status'    => null,
		'post_mime_type' => 'image',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		// 'linked' => $link, // link = create link
	))) {
		foreach ( $images as $image ) {
			$attimg   = wp_get_attachment_image($image->ID,$size);
			$atturl   = wp_get_attachment_url($image->ID);
			$attlink  = get_attachment_link($image->ID);
			//$postlink = get_permalink($image->post_parent);
			//$atttitle = apply_filters('the_title',$image->post_title);

			//echo '<div class="image image-'.$size.'">'; // thumb needed for galleriffic
			if ( $list == 'li') { echo '<li>'; };
			if ( $link == 'link') {  echo '<a href="'.$atturl.'" class="thumb">';  };
			//if ($link == no) {echo 'no link'} ;  
			//end if;
			echo $attimg;
			if ( $link == 'link') {  echo '</a>';  };
			if ( $list == 'li') { echo '</li>'; };
			//echo '</div>';
		}
	}
}


// file attachments

// get the first PDF attached to the current post
function kk_get_pdf() {
	global $post;
// used in: 
// - single.php
	if($attachments = get_children(array(
		'post_parent'    => get_the_ID(),
		'post_type'      => 'attachment',
		'numberposts'    => -1, // -1 = show all
		'post_status'    => null,
		'post_mime_type' => 'application/msword,application/pdf',
		'orderby' => 'menu_order',
		'order' => 'ASC',
	))) {
		foreach($attachments as $attachment) {
			// $attimg   = wp_get_attachment_image($attachment->ID,$size);
			$atturl   = wp_get_attachment_url($attachment->ID);
			// $attlink  = get_attachment_link($attachment->ID);
			// $postlink = get_permalink($attachment->post_parent);
			$atttitle = apply_filters('the_title',$attachment->post_title);
			// $description = $attachment->post_content;
			
			echo '<div class="attach-doc"><span class="attach-title">'.$atttitle.'</span>';
			// if($description !== '') { echo '<span class="descr"> - '.$description.'</span>'; } ;
			echo '<a href="'.$atturl.'" class="attach-link" title="PDF Herunterladen">PDF</a></div>';		
		}
	}
	else {		
		
	}
}

// minimal PDF display

function kk_pdf_minimal() {
	global $post;
// used in: 
// - single.php
	if($attachments = get_children(array(
		'post_parent'    => get_the_ID(),
		'post_type'      => 'attachment',
		'numberposts'    => 1, // -1 = show all
		'post_status'    => null,
		'post_mime_type' => 'application/pdf',
		'orderby' => 'menu_order',
		'order' => 'ASC',
	))) {
		foreach($attachments as $attachment) {
			$atturl   = wp_get_attachment_url($attachment->ID);
			
			echo '<li><a href="'.$atturl.'" class="attach-link-minimal" title="PDF Herunterladen">PDF</a></li>';		
		}
	}
	else {		
		
	}
}




// sanitize the gallery style
// see http://core.trac.wordpress.org/ticket/10734

add_filter( 'use_default_gallery_style', '__return_false' );

// http://wpengineer.com/1802/a-solution-for-the-wordpress-gallery/

//deactivate WordPress function
remove_shortcode('gallery', 'gallery_shortcode');
//activate own function
add_shortcode('gallery', 'n3kr_gallery_shortcode');
//the own renamed function
function n3kr_gallery_shortcode($attr) {
	global $post, $wp_locale;

	static $instance = 0;
	$instance++;

	// Allow plugins/themes to override the default gallery template.
	$output = apply_filters('post_gallery', '', $attr);
	if ( $output != '' )
		return $output;

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post->ID,
		'itemtag'    => 'dl',
		'icontag'    => 'dt',
		'captiontag' => 'dd',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => ''
	), $attr));

	$id = intval($id);
	if ( 'RAND' == $order )
		$orderby = 'none';

	if ( !empty($include) ) {
		$include = preg_replace( '/[^0-9,]+/', '', $include );
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
		return '';

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment )
			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		return $output;
	}

	$itemtag = tag_escape($itemtag);
	$captiontag = tag_escape($captiontag);
	$columns = intval($columns);
	//$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = $gallery_div = '';
	if ( apply_filters( 'use_default_gallery_style', true ) )
		$gallery_style = "
		<style type='text/css'>
			#{$selector} {
				margin: auto;
			}
			#{$selector} .gallery-item {
				float: {$float};
				margin-top: 10px;
				text-align: center;
				width: {$itemwidth}%;
			}
			#{$selector} img {
				border: 2px solid #cfcfcf;
			}
			#{$selector} .gallery-caption {
				margin-left: 0;
			}
		</style>
		<!-- see gallery_shortcode() in wp-includes/media.php -->";
	$size_class = sanitize_html_class( $size );
	$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
	$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {
		$link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);

		$output .= "<{$itemtag} class='gallery-item'>";
		$output .= "
			<{$icontag} class='gallery-icon'>
				$link
			</{$icontag}>";
		if ( $captiontag && trim($attachment->post_excerpt) ) {
			$output .= "
				<{$captiontag} class='wp-caption-text gallery-caption'>
				" . wptexturize($attachment->post_excerpt) . "
				</{$captiontag}>";
		}
		$output .= "</{$itemtag}>";
		if ( $columns > 0 && ++$i % $columns == 0 )
			$output .= ' ';
	}

	$output .= "</div>\n";

	return $output;
}



// HTML5 Theme Functions
// Custom HTML5 Comment Markup
function mytheme_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li>
     <article <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
       <header class="comment-author vcard">
          <?php echo get_avatar($comment,$size='48',$default='<path_to_url>' ); ?>
          <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
          <time><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a></time>
          <?php edit_comment_link(__('(Edit)'),'  ','') ?>
       </header>
       <?php if ($comment->comment_approved == '0') : ?>
          <em><?php _e('Your comment is awaiting moderation.') ?></em>
          <br />
       <?php endif; ?>

       <?php comment_text() ?>

       <nav>
         <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
       </nav>
     </article>
    <!-- </li> is added by wordpress automatically -->
<?php
}



// Custom Functions for CSS/Javascript Versioning
$GLOBALS["TEMPLATE_URL"] = get_bloginfo('template_url')."/";
$GLOBALS["TEMPLATE_RELATIVE_URL"] = wp_make_link_relative($GLOBALS["TEMPLATE_URL"]);

// Add ?v=[last modified time] to style sheets
function versioned_stylesheet($relative_url, $add_attributes=""){
  echo '<link rel="stylesheet" href="'.versioned_resource($relative_url).'" '.$add_attributes.'>'."\n";
}

// Add ?v=[last modified time] to javascripts
function versioned_javascript($relative_url, $add_attributes=""){
  echo '<script src="'.versioned_resource($relative_url).'" '.$add_attributes.'></script>'."\n";
}

// Add ?v=[last modified time] to a file url
function versioned_resource($relative_url){
  $file = $_SERVER["DOCUMENT_ROOT"].$relative_url;
  $file_version = "";

  if(file_exists($file)) {
    $file_version = "?v=".filemtime($file);
  }

  return $relative_url.$file_version;
}

/* some cleanup */

remove_action('wp_head', 'shortlink_wp_head');

// Prevents WordPress from testing ssl capability on domain.com/xmlrpc.php?rsd
remove_filter('atom_service_url','atom_service_url_filter');

function keep_me_logged_in_for_1_year( $expirein ) {
   return 31556926; // 1 year in seconds
}
add_filter( 'auth_cookie_expiration', 'keep_me_logged_in_for_1_year' );


// vimeo helper function
// Curl helper function
function curl_get($url) {
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
	$return = curl_exec($curl);
	curl_close($curl);
	return $return;
}

// wp_deregister_script('jquery');
// problem: also disables it in the admin ...

/*admin interface*/

/**
 * remove WordPress Howdy
 * http://www.redbridgenet.com/?p=653
 */


/* source: 
http://devpress.com/blog/fixing-wordpress-3-2s-html-editor-font/
*/

//add_action( 'admin_head-post.php', 'devpress_fix_html_editor_font' );
//add_action( 'admin_head-post-new.php', 'devpress_fix_html_editor_font' );

function devpress_fix_html_editor_font() { ?>
<style type="text/css">#editorcontainer #content, #wp_mce_fullscreen { /* font-family: Georgia, "Times New Roman", "Bitstream Charter", Times, serif;  */ }</style> <?php }

remove_action( 'wp_head', 'feed_links' ); // not working...
remove_action( 'wp_head', 'feed_links', 2 );
remove_action('wp_head','feed_links_extra', 3);
// in order to remove the comments feed. need to add manually the main RSS feed to the header.

remove_action( 'wp_head', 'wp_generator');

// Admin Interface improvement
function kk_admin_styles() {
   echo '<style type="text/css">
           #wp-admin-bar-new-page,
           #wp-admin-bar-new-link,
           #wp-admin-bar-new-media
           {display:none}
           
           
           </style>';
   // add those styles also to main stylesheet
}

add_action('admin_head', 'kk_admin_styles');



/* Allowed FileTypes
 ********************
 * method based on 
 * http://howto.blbosti.com/?p=329
 * List of defaults: https://core.trac.wordpress.org/browser/tags/3.8.1/src/wp-includes/functions.php#L1948
*/
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {

		// add an extension to the array
		$existing_mimes['svg'] = 'image/svg+xml';

		// remove existing file types
		unset( $existing_mimes['bmp'] );
		unset( $existing_mimes['tif|tiff'] );

		// and return the new full result
		return $existing_mimes;
}


/*
 * File Upload Security
 
 * Sources: 
 * http://www.geekpress.fr/wordpress/astuce/suppression-accents-media-1903/
 * https://gist.github.com/herewithme/7704370
 
 * See also Ticket #22363
 * https://core.trac.wordpress.org/ticket/22363
 * and #24661 - remove_accents is not removing combining accents
 * https://core.trac.wordpress.org/ticket/24661
*/ 

add_filter( 'sanitize_file_name', 'remove_accents', 10, 1 );
add_filter( 'sanitize_file_name_chars', 'sanitize_file_name_chars', 10, 1 );
 
function sanitize_file_name_chars( $special_chars = array() ) {
	$special_chars = array_merge( array( '’', '‘', '“', '”', '«', '»', '‹', '›', '—', 'æ', 'œ', '€','é','à','ç','ä','ö','ü','ï','û','ô','è' ), $special_chars );
	return $special_chars;
}


// end of functions.php