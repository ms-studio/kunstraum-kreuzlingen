<?php 

/***************************************** 
 	* LOOP Nr 3.
	* We look for attached materials
	* Videos or other...
 *****************************************/
 
 // we need to test for this:
 // $current_meta = 'Video-expo-clip';
	
 // $kk_vimeo_key = get_post_meta($post->ID, 'Video-expo-clip', true);
 
// Turn off all error reporting
	error_reporting(0);
// for Infomaniak...
// http://php.net/manual/en/function.error-reporting.php

	// test for meta field
	
	if ($kk_vimeo_key) { 
	
	// YES, we have videos
	// lets build the vimeo query

	if (function_exists('vimeovortex')) {

		$video = vimeovortex_array($kk_vimeo_key);

		// echo '<pre>';
		// var_dump($video);
		// echo '</pre>';
		
		$video_img = $video["thumbnail_url"];
		$video_img = str_replace("http:","https:",$video_img);
						    			
	
	// fix the URLs for every case
	// we want ONLY the NUMBER...
	
	$kk_vimeo_key = str_replace("http://vimeo.com/", "", $kk_vimeo_key);
	$kk_vimeo_key = str_replace("https://vimeo.com/", "", $kk_vimeo_key);
	$kk_vimeo_key = rtrim($kk_vimeo_key,"/");

	
	?>
	
	<div class="list-item list-item-video list-item-<?php 
				
				echo $exhib_loc;
		
			?> list-grid-system">
	
	<a href="https://vimeo.com/<?php echo $kk_vimeo_key; ?>" data-vimeo="<?php echo $kk_vimeo_key; ?>" target="_blank" title="<?php the_title(); ?>" id="post-<?php the_ID(); ?>" class="list-item-inside dblock ajaxtrigger-vimeo unstyled" data-location="style-<?php echo $exhib_loc; ?>">
	
	<div class="vimeo-play-icon"></div>
	
	<?php 
	
		echo '<img class="vimeo-thumbnail" src="'. $video_img .'" alt="" width="'. $video["video"]["width"] .'" height="'. $video["video"]["height"] .'" />';
	
	
	?></a>
	
	<p class="list-item-title small-font">
		
		<?php 
		
		if ($exhib_status == 'vorschau') {
		
		echo '<span class="expo-vorschau-prefix prefix">vorschau</span> ';
		
		} elseif ($exhib_status == 'aktuell') {
		
		echo '<span class="expo-aktuell-prefix prefix">aktuell</span> ';
		
		}
		
		?>
	
		<?php echo $kk_artist_name; ?> 
	
		<a href="<?php echo $exhib_url; ?>" class="exhib-link"><?php 
			echo $exhib_title; 
		?></a>
	</p>
	
	</div><!-- .exhib-loc -->

	<?php

	} // function_exists('vimeovortex')

 } // if there is no meta field...
 
else { 

// nothing happens

 } 

// end of connected MATERIALS
// end of LOOP Nr 3.
// *********************
?>
