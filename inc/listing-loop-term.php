<?php 

/***************************************** 
 	* LOOP Nr 3.
	* We look for attached materials
	* Synopse or other...
 *****************************************/
 
 // NOTE: 
 // things to improve:
 // prevent duplicates (if one material is attached to several exhibits, it may appear twice)
 // solution: we need to add the post ID to an array, and test...
	
$connected_syn = new WP_Query( array(
  'posts_per_page' => -1,
  'connected_type' => 'materials_to_posts',
  'connected_items' => $this_post_id, // get_queried_object_id(),
  'post_type' => 'kk_material',
  'post_status' => array ('publish', 'future'),	    
  'orderby' => 'date',
  'order' => 'DESC', // desc = newest first
  'tax_query' => array(
  		array(
  			'taxonomy' => 'material_types',
  			'field' => 'slug',
  			'terms' => $current_term,
  			// options: synopse, tribune de critique...
  			//'operator' => 'NOT IN'
  		)
  	),
) );

// Display connected pages

if ( $connected_syn->have_posts() ) :

// YES, we have some MATERIALS ...
?>

<?php 

while ( $connected_syn->have_posts() ) : $connected_syn->the_post(); 
// test variables

			
$synopse_id = get_the_ID();
$synopse_url = get_permalink();
?>

<div class="list-item list-item-<?php 
			
			echo $exhib_loc;
	
		?> list-grid-system">

	<!-- <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" id="post-<?php the_ID(); ?>"><?php the_title(); ?></a> -->
	
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" id="post-<?php the_ID(); ?>" class="list-item-inside dblock norm-font unstyled ajaxtrigger" data-location="style-<?php echo $exhib_loc; ?>"><?php 
						 // echo $itemloop;
						 // echo " / ";
						the_excerpt(); 
	?></a>
	
	<p class="list-item-title small-font">
		
		<?php 
		
		if ($exhib_status == 'vorschau') {
		
		echo '<span class="expo-vorschau-prefix prefix">vorschau</span> ';
		
		} elseif ($exhib_status == 'aktuell') {
		
		echo '<span class="expo-aktuell-prefix prefix">aktuell</span> ';
		
		}
		
		?>
	
		<?php echo $kk_artist_name; ?> 
	
		<a href="<?php echo $exhib_url; ?>" class="exhib-link"><?php 
			echo $exhib_title; 
		?></a>
	</p>
	
	</div><!-- .exhib-loc -->
	
<?php 
endwhile; 
?>
<?php 
else : ?>
<?php 
endif; 
// end of connected MATERIALS
// end of LOOP Nr 3.
// *********************
?>
