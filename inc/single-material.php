<!-- Template: single-material.php -->
 
  <article id="mainframe" <?php post_class('mainframe single-article') ?> data-postid="post-<?php the_ID(); ?>">
  	<!-- Template: single-material.php -->
	  
    	  <?php
    	  
    	    $connected = new WP_Query( array(
    	      'connected_type' => 'materials_to_posts',
    	      'connected_items' => get_queried_object_id(),
    	    ) );
    	    
    	    // Display connected pages - EXPOS
    	    if ( $connected->have_posts() ) :
    	    ?>
    	    <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
    	    	<h3 class="medium-font related mainframe-content mainframe-header non-ajax">Zur Ausstellung: <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    	    <?php 
    	    
    	    // we may also define variables here: 
    	    // ARTIST NAME, 
    	    // EXHIBITION DATE
    	    
    	    $kk_related_expo = get_the_ID();
    	    
    	    // echo $kk_related_expo;
    	    
    	    $kk_artist_name = get_post_meta($post->ID, 'Künstler-Name', true);
    	    
    	    $kk_date = get_post_meta($post->ID, 'Datum', true);
    	    
    	    if($kk_artist_name == '') {
    	    
    	    // test for: artist name meta content
    	    // else:  artist name p2p connection
    	    
    	    /***************************************** 
    	     	* LOOP Nr 2.
    	    	* Find connected material - Artist
    	     *****************************************/
    	    	
	    	    $connected_artist = new WP_Query( array(
	    	    	'posts_per_page' => 1,
	    	      'connected_type' => 'posts_to_kuenstler',
	    	      'connected_items' => $kk_related_expo,
	    	      'post_type' => 'kuenstler',
	    	    ) );
	    	    
	    	    if ( $connected_artist->have_posts() ) :
	    	    while ( $connected_artist->have_posts() ) : $connected_artist->the_post(); 
	    	    
	    	    // set variable
	    	    $kk_artist_name = get_the_title();
	    	    // $kk_artist_url = get_permalink(); NOT NEEDED
	    	    	    	    
	    	    endwhile; 
	    	    	wp_reset_postdata();
	    	    else :
    	    			// echo "no connected artist";
    	    endif; // END connected ARTIST query
    	    
    	    } // END test for $kk_artist_name 
    	    
    	    	endwhile; 
    	    
    	    // Prevent weirdness
    	    	wp_reset_postdata();
    	    	 endif; // END connected pages
    	    // } // end AJAX test
    	    
    	    /*
    	    *****************************
    	    *************/
    	    
    	    // now : test for 'Kategorie' in order to show or hide 
    	    // title, PDFs, print button, etc
    	    
    	    // styling with title for: Synopse, Tribune-de-Critique
    	    // with no title for: Presse, Publication
    	    
    	    if ( has_term( array( 'synopsen' ), 'material_types' ) ) {
    	    
    	    	// we want to show: 
    	    	
    	    	// * Artist name
    	    	// * Title
    	    	// * Date
    	    	
    	    	// * content
    	    	
    	    	// * PDFs attachments
    	    
    	    		 ?>
    	    		 <header class="mainframe-content mainframe-header">
    	    		 	<?php // echo $kk_artist_name;
    	    		 		if($kk_artist_name !== '') {
    	    		 			echo '<h2 class="h2 kuenstler-name margin-zero">';
    	        		 		echo $kk_artist_name;
    	        		 		echo '</h2>';
    	        		 } ?>
    	    		   <h1 class="main-title h1 italic-title"><?php the_title(); ?></h1>
    	    		   <?php //
    	    		   		if($kk_date !== '') {
    	    		   			echo '<p class="datum">';
    	    		   			echo $kk_date;
    	    		   			echo '</p>';
    	    		   		} ?>
    	    		   		
    	    		   	<div class="print-pdf">
    	    		   	<ul class="clean ul-print-pdf">
    	    		   	<?php kk_pdf_minimal(); ?>
    	    		   <li><a class="print-button" href="#" onClick="window.print();return false">Drucken</a></li>
    	    		   	</div>
    	    		 </header>
    	    		 
    	    		 <div class="mainframe-content larger-font mf-content-text">
    	    		     <div class="main-content indented">
    	    		     	<?php the_content('Read the rest of this entry &raquo;'); ?>
    	    		     </div>
    	    		 	
    	    		 	
    	    		 </div><!-- .mainframe-content -->
    	    		 <?php
    	    		 
    	    	} elseif ( has_term( array( 'tribune-de-critique', 'reden' ), 'material_types' ) ) {
    	    
    	    	// we want to show: 
    	    	
    	    	// * Title
    	    	// * Sub-title - Titel-Zweite-Zeile
    	    	// * content
    	    	// * PDFs attachments
    	    	
    	    	$kk_subtitle = get_post_meta($post->ID, 'Titel-Zweite-Zeile', true);
    	    
    	    		 ?>
    	    		 <header class="mainframe-content mainframe-header">
    	    		 	
    	    		   <h1 class="main-title h1 italic-title"><?php the_title(); ?></h1>
    	    		   <?php // Zweite Zeile
    	    		   	if($kk_subtitle !== '') {
    	    		   		echo '<h2 class="h2 margin-zero">';
    	    		   		echo $kk_subtitle;
    	    		   		echo '</h2>';
    	    		   } ?>
    	    		   
    	    		   	<div class="print-pdf">
    	    		   	<ul class="clean ul-print-pdf">
    	    		   	<?php kk_pdf_minimal(); ?>
    	    		   <li><a class="print-button" href="#" onClick="window.print();return false">Drucken</a></li>
    	    		   	</div>
    	    		 </header>
    	    		 
    	    		 <div class="mainframe-content larger-font mf-content-text">
    	    		     <div class="main-content indented">
    	    		     	<?php the_content('Read the rest of this entry &raquo;'); ?>
    	    		     </div>
    	    		 	
    	    		 	
    	    		 </div><!-- .mainframe-content -->
    	    		 <?php
    	    		 
    	    	} elseif ( has_term( array( 'presse', 'publikation' ), 'material_types' ) ) {
    	    	
    	    	// we want to show: content, PDFs
    	    	
    	    			?>
    	    			<div class="mainframe-content larger-font">
    	    			    <div class="main-content">
    	    			    	<?php the_content('Read the rest of this entry &raquo;'); ?>
    	    			    </div>
    	    				
    	    				<div class="single-attachments">
    	    					<?php kk_get_pdf(); ?>
    	    				</div>
    	    			</div><!-- .mainframe-content -->
    	    			<?php
    	    	} else {
    	    	?>
    	    	<header class="mainframe-content mainframe-header">
    	    	  <h1 class="main-title h1"><?php the_title(); ?></h1>
    	    	</header>
    	    	
    	    	<div class="mainframe-content larger-font">
    	    	    <div class="main-content">
    	    	    	<?php the_content('Read the rest of this entry &raquo;'); ?>
    	    	    </div>
    	    		
    	    		<div class="single-attachments">
    	    			<?php kk_get_pdf(); ?>
    	    		</div>
    	    	</div><!-- .mainframe-content -->
    	    	<?php
    	    	} // end conditions
    	    ?> 
	<?php edit_post_link('bearbeiten', '<p class="edit">[ ', ' ]</p>'); ?>
  </article>


