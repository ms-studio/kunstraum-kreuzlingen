<!-- Template: single-expo-nav.php -->
   
  	<nav class="nav-expos clearfix small-font">
	  	<ul class="horiz-list default-menu">
	  		<li><a href="<?php bloginfo('url'); ?>/ausstellungen/" id="a-uebersicht" class="a-uebersicht">Übersicht</a></li>
	  	</ul>
	  	
	  	<div id="k-liste" class="k-liste">
	  		<h1 class="menu-title"><span class="border-bottom">Künstler A - Z</span></h1>
	  	
	  	<?php 
	  		
	  		$kk_artist_menu = kk_build_artist_menu();
	  		
	  		echo $kk_artist_menu;
	  			  	
	  	 ?>
	  	 
	  	 </div>
	  	
	  	<ul class="horiz-list default-menu rightfloat kunstr-parterre">
	  		
	  		<?php 	//check if we are in Kunstraum : 
	  				//if yes, output related Exhibit
	  				if ( in_category( 'kunstraum' ) ) {
	  					 ?>
	  					 
	  					 <li class="kunstraum-link active current-menu-item"><a href="#" title="<?php the_title(); ?>">Kunstraum</a></li>
	  					 <?php
	  					   // Find connected pages
	  					   $connected = new WP_Query( array(
	  					     'connected_type' => 'posts_to_posts',
	  					     'category_name' => 'tiefparterre',
	  					     'connected_items' => get_queried_object_id(),
	  					     'posts_per_page' => 1,
	  					   ) );
	  					   
	  					   // Display connected pages
	  					   if ( $connected->have_posts() ) :
	  					   ?>
	  					   <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
	  					   	<li class="tiefparterre-link"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Tiefparterre</a></li>
	  					   <?php endwhile; ?>
	  					   
	  					   <?php 
	  					   // Prevent weirdness
	  					   wp_reset_postdata();
	  					   
	  					   endif; //connected pages
	  					   ?>
	  					 
	  					 <?php
	  				}
	  				else {  // we are 'tiefparterre'
	  						
	  		  // Find connected Kunstraum page
	  		  $connected = new WP_Query( array(
	  		    'connected_type' => 'posts_to_posts',
	  		    'category_name' => 'kunstraum',
	  		    'connected_items' => get_queried_object_id(),
	  		    'posts_per_page' => 1,
	  		  ) );
	  		  
	  		  // Display connected pages
	  		  if ( $connected->have_posts() ) :
	  		  ?>
	  		  <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
	  		  	<li class="kunstraum"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Kunstraum</a></li>
	  		  <?php endwhile;
	  		  // Prevent weirdness
	  		  wp_reset_postdata();
	  		  
	  		  endif; //connected pages
	  		  ?>
	  		</li>
	  		<li class="tiefparterre active current-menu-item"><a href="#" title="<?php the_title(); ?>">Tiefparterre</a></li>
	  			<?php
	  				} //end if-else
	  			; ?> 
	  		</ul>
	  	
	  	
	  	<div class="archive-container archive-container-<?php the_time('Y') ?>">
	  	
	  	
	  	<div class="next-prev next unstyled-plus" title="Ältere Ausstellungen"><?php previous_post_link('%link', '&larr;') ?></div>
	  	
	  		<div id="archive-box" class="archive-box">
	  	 <?php
	  	  		  // Declare some helper vars
	  	  		  $previous_year = $year = 0;
	  	  		  $previous_month = $month = 0;
	  	  		  $ul_open = false;
	  	  		  
	  	  		  if ( in_category( 'kunstraum' ) ) {
	  	  		  	$search_category = 3;
	  	  		  }
	  	  		  else {  // we are 'tiefparterre'
	  	  		  	$search_category = 4;
	  	  		  }
	  	  		  // Get the posts
		  	  		$metronom = 0 ;
		  	  		
	  	  		  $args = array( 
	  	  		  	//'category' => $search_category, 
	  	  		  	'category' => '3,4', 
	  	  		  	'numberposts' => -1, 
	  	  		  	'orderby' => 'post_date', 
	  	  		  	'order' => 'ASC', 
	  	  		  	//'order' => 'DESC',
	  	  		  	); 
	  	  		  	
	  	  		  $myposts = get_posts($args);
	  	  		  
				 foreach($myposts as $post) : 
				 
				 // Setup the post variables
	  	  		  	setup_postdata($post);
	  	  		   
	  	  		  	$year = mysql2date('Y', $post->post_date);
	  	  		  	$month = mysql2date('n', $post->post_date);
	  	  		  	$day = mysql2date('j', $post->post_date);
	  	  		  	
	  	  		  	//$metronom++;
	  	  		  	
	  	  		  	 if($year != $previous_year ) : 
	  	  		  	 	if($ul_open == true) : 
	  	  		  	 	
	  	  		  	 	$metronom = 0 ;
	  	  		  	 	?>
	  	  		  		</ul></div>
	  	  		  		<?php endif; ?>
	  	  		   		
	  	  		   		<div class="year-archive year-archive-<?php the_time('Y') ?>">
	  	  		  		<h3 class="unstyled title-year-archive title-year-archive-<?php echo $year; ?>"><span class="a"><?php echo $year; ?></span></h3>
	  	  		  		<ul class="ul-year-archive ul-year-archive-<?php echo $year; ?> hidden unstyled">
	  	  		   
	  	  		  		<?php $ul_open = true; ?>	  	  		   
	  	  		  	<?php endif; ?>
	  	  		  	<?php $previous_year = $year; $previous_month = $month; 
	  	  		  	
	  	  		  			$metronom++;
	  	  		  	?>
	  	  		  	<li>
	  	  		  		<span class="the_article"><a href="<?php 
	  	  		  		the_permalink(); 
	  	  		  		?>" class="<?php
	  	  		  		
	  	  		 $titleslug = basename(get_permalink()); 
	  	  		 echo $titleslug; ?>" title="<?php 
							
							$current_expo_title = get_the_title();
							echo esc_attr( $current_expo_title );
							
							
	  	  		 ?>"><?php
	  	  		 			
	  	  		 			// echo $metronom . ' ';
	  	  		 			
	  	  		 			if ( in_category( 'vorschau' ) ) {
		  	  		 		
		  	  		 			?><span class="expo-vorschau-prefix">vorschau</span><?php
	  	  		 			
	  	  		 			} elseif ( in_category( 'aktuell' ) ) {
	  	  		 			
	  	  		 				?><span class="expo-aktuell-prefix">aktuell</span><?php
	  	  		 			
	  	  		 			}
	  	  		 			
	  	  		 			
	  	  		 			// new: full output
	  	  		 			$kk_kuenstler = get_post_meta($post->ID, 'Künstler-Name', true);
	  	  		 			if ($kk_kuenstler) {
	  	  		 				echo '<span class="kuenstler-name">'.$kk_kuenstler . '</span>';
	  	  		  		} else {
	  	  		  		
	  	  		  			// check for P2P
	  	  		  			
	  	  		  			$connected_stuff = new WP_Query( array(
	  	  		  			   'posts_per_page' => -1,
	  	  		  			   'connected_type' => 'posts_to_kuenstler',
	  	  		  			   'post_type' => 'kuenstler',
	  	  		  			   'nopaging' => true,
	  	  		  			   'connected_items' => get_the_ID(), 
	  	  		  			   // get_queried_object_id(), // $this_post_id
	  	  		  			   'order' => 'DESC', // desc = newest first
	  	  		  			 ) );
	  	  		  			 
	  	  		  			 // Display connected PROJECTS
	  	  		  			 if ( $connected_stuff->have_posts() ) :
	  	  		  			      
	  	  		  			 	     while ( $connected_stuff->have_posts() ) : $connected_stuff->the_post(); 
	  	  		  			 	     
	  	  		  			 	     echo '<span class="kuenstler-name">'.get_the_title(). '</span>';
	  	  		  			 	      
	  	  		  			 	     endwhile;
	  	  		  			     
	  	  		  			 // Prevent weirdness
	  	  		  			 wp_reset_postdata();
	  	  		  			 endif;
	  	  		  			 // end stuff
	  	  		  		}
	  	  		  		
	  	  		  		// title 
	  	  		  		
	  	  		  		echo '<span class="expo-title ital">';
	  	  		  		echo $current_expo_title;
	  	  		  		echo '</span>';
	  	  		  		
	  	  		  		?></a></span>
	  	  		  	</li>	  	  		   
	  	  		  <?php endforeach;
	  	  		  // Prevent weirdness
	  	  		  wp_reset_postdata();
	  	  		   ?>
	  	  		  	</ul>
	  	  		  	</div>
	  	  		  </div><!-- archive-box -->
	  	  		  
	  	  		  <div class="next-prev prev unstyled-plus" title="Neuere Ausstellungen"><?php next_post_link('%link', '&rarr;') ?></div>
	  	  </div><!-- archive-container -->
	  			
  	</nav>
  

