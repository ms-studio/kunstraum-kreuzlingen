<!-- Template: single-article.php -->
 
  <article id="mainframe" <?php post_class('mainframe single-article') ?> title="post-<?php the_ID(); ?>">
  
	  
	  
    <header class="mainframe-content mainframe-header">
    
    	<?php
    	
    	if ( 'post' == get_post_type() ) {
    	
    	  // Find connected pages - Kuenstler
    	  
    	  // error because there is no connection ...
    	  $connected = new WP_Query( array(
    	    'connected_type' => 'posts_to_kuenstler',
    	    'connected_items' => get_queried_object_id(),
    	    'post_type' => 'kuenstler',
    	  ) );
    	  
    	  // Display connected pages
    	  if ( $connected->have_posts() ) :
    	  ?>
    	  <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
    	  	<h3 class="medium-font related"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>:</h3>
    	  <?php endwhile; ?>
    	  
    	  <?php 
    	  // Prevent weirdness
    	  wp_reset_postdata();
    	  
    	  endif; //connected pages
    	  ?>
    	  
    	  
    	  <?php
    	    // Find connected pages - Exhibit
    	    // this part works in p2p 1.1.4
    	    $connected = new WP_Query( array(
    	      'connected_type' => 'materials_to_posts',
    	      'connected_items' => get_queried_object_id(),
    	    ) );
    	    
    	    // Display connected pages
    	    if ( $connected->have_posts() ) :
    	    ?>
    	    <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
    	    	<h3 class="medium-font related">Zur Ausstellung: <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    	    <?php endwhile; ?>
    	    
    	    <?php 
    	    // Prevent weirdness
    	    wp_reset_postdata();
    	    
    	    endif; //connected pages
    	    
    	 } // end testing for ( 'post' == get_post_type() )
    	    ?>
    
      <h1 class="main-title h1"><?php the_title(); ?></h1>
            
    </header>
    
    <div class="mainframe-content">
	    <div class="main-content medium-font">
	    	<?php the_content('Read the rest of this entry &raquo;'); ?>
	    </div>
		
		<div class="right-block post-meta">
		
		<?php // check for meta fields : Title, Year, Type ...
				$ms_year = get_post_meta($post->ID, 'Year', true);
				$ms_type = get_post_meta($post->ID, 'Type', true);
		?>

							
		</div><!-- .right-block -->
    </div><!-- .full-block -->

  </article>


