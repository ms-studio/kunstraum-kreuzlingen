<!-- Template: single-expo.php -->
 
  <div <?php post_class('mainframe single-expo') ?> id="post-<?php the_ID(); ?>">
  
  	<?php include( TEMPLATEPATH . '/inc/single-expo-nav.php' ); ?>
  
	<article >
	
	<div id="exhib-block-m" class="exhib-block-m exhib-block-intro" >
	  
	    <header>
	    
	    <?php // check for Cat Vorschau 
	    			if ( in_category( 'vorschau' ) ) {
	    ?><div class="expo-vorschau"><span class="prefix small-font">Vorschau</span></div><?php
	    	   }
	    ?>
	    	    
	    <?php // check for meta fields
	    	$kk_kuenstler = get_post_meta($post->ID, 'Künstler-Name', true);
	    	$current_ID = ($post->ID);
	    
	     if($kk_kuenstler !== '') {
	    		echo '<h2 class="h2 kuenstler-name">';
	    		echo $kk_kuenstler;
	    		echo '</h2>';
	    		} else { 
	    		//echo the_time('Y'); 
	    		
	    	  // Find connected pages - Kuenstler
	    	  $connected = new WP_Query( array(
	    	    'connected_type' => 'posts_to_kuenstler',
	    	    'connected_items' => get_queried_object_id(),
	    	  ) );
	    	  
	    	  // Display connected pages
	    	  
	    	  if ( $connected->have_posts() ) :
	    	  ?>
	    	  <?php while ( $connected->have_posts() ) : $connected->the_post(); 
	    	  
		    	  echo '<h2 class="h2 kuenstler-name">';
		    	  the_title();;
		    	  echo '</h2>';
	    	  
	    	  endwhile; ?>
	    	  
	    	  <?php 
	    	  // Prevent weirdness
	    	  wp_reset_postdata();
	    	  
	    	  endif; //connected pages
	    	  
	    	  } // end else
	    	  
	    	  ?>
	    
	      <h1 class="h2 ital"><?php the_title(); ?></h1>
	            
	    </header>
    
	    <div class="main-content small-font">
	    
	    <?php // check for meta fields : Title, Year, Type ...
	    		$kk_date = get_post_meta($post->ID, 'Datum', true);
	    		if($kk_date !== '') {
	    			echo '<p class="datum">';
	    			echo $kk_date;
	    			echo '</p>';
	    		} ?>
	    
	    	<?php the_content('Read the rest of this entry &raquo;'); ?>
	    	
	    <ul id="connected-material" class="connected-material clean unstyled hoverable">
	    	<?php 
	    	
	    	// We will start defining some variables...
	    	
	    	$synopse_id = '';
	    	$tribune_id = '';
	    	$kk_rede_id = '';
	    	$events_id = '';
	    	$presse_id = '';
	    	$publikation_id = '';
	    	
	    	// Find connected material - Synopse
	    	$connected = new WP_Query( array(
	    	  'posts_per_page' => 1,
	    	  'connected_type' => 'materials_to_posts',
	    	  'connected_items' => get_queried_object_id(),
	    	  'post_type' => 'kk_material',
	    	  'post_status' => array ('publish', 'future'),	    
	    	  'orderby' => 'date',
	    	  'order' => 'ASC',
	    	  'tax_query' => array(
	    	  		array(
	    	  			'taxonomy' => 'material_types',
	    	  			'field' => 'slug',
	    	  			'terms' => 'synopsen',
	    	  			//'operator' => 'NOT IN'
	    	  		)
	    	  	),
	    	) );
	    	// Display connected pages
	    	if ( $connected->have_posts() ) :
	    	while ( $connected->have_posts() ) : $connected->the_post(); 
	    	// define variables
	    	$synopse_id = get_the_ID();
	    	$synopse_url = get_permalink();
	    	?>
	    		<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" id="post-<?php the_ID(); ?>">Synopse</a>
	    		</li>
	    	<?php endwhile; ?>
	    	<?php // Prevent weirdness
	    	wp_reset_postdata();
	    	else : ?>
	    	<?php 
	    	endif; //connected SYNOPSE
	    	?>
	    	
	    	<?php 
	    	// Find connected TDC
	    	global $connected_tdc;
		    	$connected_tdc = new WP_Query( array(
		    	  'posts_per_page' => -1,
		    	  'connected_type' => 'materials_to_posts',
		    	  'connected_items' => get_queried_object_id(),
		    	  'post_type' => 'kk_material',
		    	  'post_status' => array ('publish', 'future'),	    
		    	  'orderby' => 'date',
		    	  'order' => 'ASC',
		    	  'tax_query' => array(
		    	  		array(
		    	  			'taxonomy' => 'material_types',
		    	  			'field' => 'slug',
		    	  			'terms' => 'tribune-de-critique',
		    	  		)
		    	  	),
		    	) );
		    	
		    	$tribune_nr = ($connected_tdc->post_count);
		    	$post_count = 1;
		    	
		    	// Display connected pages (only first)
		    	if ( $connected_tdc->have_posts() ) :
		    	
				while ( $connected_tdc->have_posts()  && ($post_count < 2) ) : $connected_tdc->the_post();
				$post_count++; 
		    	// define variables
		    	$tribune_id = get_the_ID();
		    	$tribune_url = get_permalink();
		    	$tribune_title = get_the_title();
		    	
		    	?>
		    		<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" id="post-<?php the_ID(); ?>" class="ajax-tdc permalink-tribune">Tribune de critique</a>
		    		</li>
		    	<?php endwhile; ?>
		    	<?php // Prevent weirdness
		    	wp_reset_postdata();
		    	else : ?>
		    	<?php 
		    	endif; //connected TDC
	    	?>
	    	
	    	<?php 
	    	// Find connected REDE
	    	
	    		$connected = new WP_Query( array(
	    		  'posts_per_page' => 1,
	    		  'connected_type' => 'materials_to_posts',
	    		  'connected_items' => get_queried_object_id(),
	    		  'post_type' => 'kk_material',
	    		  'post_status' => array ('publish', 'future'),	    
	    		  'orderby' => 'date',
	    		  'order' => 'ASC',
	    		  'tax_query' => array(
	    		  		array(
	    		  			'taxonomy' => 'material_types',
	    		  			'field' => 'slug',
	    		  			'terms' => 'reden',
	    		  		)
	    		  	),
	    		) );
	    		
	    		// Display connected pages
	    		if ( $connected->have_posts() ) :
	    		?>
	    		
	    		<?php while ( $connected->have_posts() ) : $connected->the_post(); 
	    		// define variables
	    		$kk_rede_id = get_the_ID();
	    		$kk_rede_url = get_permalink();
	    		?>
	    			<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" id="post-<?php the_ID(); ?>">Rede</a>
	    			</li>
	    		<?php endwhile; ?>
	    		<?php // Prevent weirdness
	    		wp_reset_postdata();
	    		else : ?>
	    		<?php 
	    		endif; //connected REDE
	    	?>
	    	
	    	<?php 
	    	// Find connected EVENTS
	    	$connected = new WP_Query( array(
	    	  'posts_per_page' => 1, // we just check if there is AT LEAST ONE event...
	    	  'connected_type' => 'materials_to_posts',
	    	  'connected_items' => get_queried_object_id(),
	    	  'post_type' => 'kk_material',
	    	  'post_status' => array ('publish', 'future'),	    
	    	  'orderby' => 'date',
	    	  'order' => 'ASC',
	    	  'tax_query' => array(
	    	  		array(
	    	  			'taxonomy' => 'material_types',
	    	  			'field' => 'slug',
	    	  			'terms' => 'events',
	    	  		)
	    	  	),
	    	) );
	    	
	    	if ( $connected->have_posts() ) :
	    	?>
	    	<?php while ( $connected->have_posts() ) : $connected->the_post(); 
	    	$events_id = get_the_ID();
	    	?>
	    	<?php endwhile; ?>
	    	<li><a id="permalink-events" href="/expo-events/?eventid=<?php echo $current_ID; ?>">Events</a></li>
	    	<?php // Prevent weirdness
	    	wp_reset_postdata();
	    	else : ?>
	    	<?php 
	    	endif; //connected EVENTS
	    	?>
	    	
	    	<?php 
	    	// Find connected  - Presse
	    	$connected = new WP_Query( array(
	    	  'posts_per_page' => 1,
	    	  'connected_type' => 'materials_to_posts',
	    	  'connected_items' => get_queried_object_id(),
	    	  'post_type' => 'kk_material',
	    	  'post_status' => array ('publish', 'future'),	    
	    	  'orderby' => 'date',
	    	  'order' => 'ASC',
	    	  'tax_query' => array(
	    	  		array(
	    	  			'taxonomy' => 'material_types',
	    	  			'field' => 'slug',
	    	  			'terms' => 'presse',
	    	  		)
	    	  	),
	    	) );
	    	
	    	if ( $connected->have_posts() ) :
	    	?>
	    	<?php while ( $connected->have_posts() ) : $connected->the_post(); 
	    	// define variables
	    	$presse_url = get_permalink();
	    	$presse_id = get_the_ID();
	    	?>
	    		<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" id="permalink-presse">Presse</a>
	    		</li>
	    	<?php endwhile; ?>
	    	<?php // Prevent weirdness
	    	wp_reset_postdata();
	    	else : ?>
	    	<?php 
	    	endif; //connected PRESSE
	    	?>

			<?php 
			// Find connected  - Publikation
			$connected = new WP_Query( array(
			  'posts_per_page' => 5,
			  'connected_type' => 'materials_to_posts',
			  'connected_items' => get_queried_object_id(),
			  'post_type' => 'kk_material',
			  'post_status' => array ('publish', 'future'),	    
			  'orderby' => 'date',
			  'order' => 'ASC',
			  'tax_query' => array(
			  		array(
			  			'taxonomy' => 'material_types',
			  			'field' => 'slug',
			  			'terms' => 'publikationen',
			  		)
			  	),
			) );
			
			if ( $connected->have_posts() ) :
			?>
			<?php while ( $connected->have_posts() ) : $connected->the_post(); 
			// define variables
			$publikation_url = get_permalink();
			$publikation_id = get_the_ID();
			?>
				<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" id="permalink-publikation">Publikation</a>
				</li>
			<?php endwhile; ?>
			<?php // Prevent weirdness
			wp_reset_postdata();
			else : ?>
			<?php 
			endif; //connected Publikation
			?>
	    	
	    </ul>
	    
	    <?php //the_meta(); ?>
	    
	    <?php $weblinks = get_post_meta($post->ID, 'Web-Link', false); 
	    // false = will return an array of results
	    if ($weblinks) { 
	    ?>
	        <div class="hyperlinks">
	    <?php foreach($weblinks as $weblink) {
	    	// test if it starts with HTTP ....
	    	// and remove it
	    	
	    $cleanlink = str_replace("http://", "", $weblink);
	    
	    echo '<div class="link"><a href="http://'.$cleanlink.'" target="_blank">'.$cleanlink.'</a></div>';
	    			} ?>
	    	</div><!-- .hyperlinks -->
	    <?php } // end if; ?>
	    
	    
	    	
	    </div><!-- .min-content -->
	    </div><!-- .exhib-block-m -->
	    
	    <div id="exhib-ajax" class="exhib-block-m hidden">
		    <header>
		    <nav id="exhib-ajax-nav" class="exhib-ajax-nav default-menu horiz-list small-font">
		    <ul class="connected-material clean">
		   		
		   		<?php 
		   		
		   		// STEP 3:
		   		// NOW we build the MENU of the AJAX container
		   		// Using variables that we defined earlier
		   		// ***********************************************/
		   		
		   		if ( $synopse_id !== '' ) { 
		   		?><li class="kunstraum permalink-synopse post-<?php echo $synopse_id ; ?>"><a href="<?php echo $synopse_url ; ?>">Synopse</a></li>
		   		<?php
		   		}
		   		
		   		if ( $tribune_id !== '' ) { 
		   		?><li class="kunstraum permalink-tribune post-<?php echo $tribune_id ; ?>"><a href="<?php echo $tribune_url ; ?>">Tribune de critique</a></li>
		   		<?php
		   		}
		   		
		   		if ( $kk_rede_id !== '' ) { 
		   		?><li class="kunstraum permalink-rede post-<?php echo $kk_rede_id ; ?>"><a href="<?php echo $kk_rede_url ; ?>">Rede</a></li>
		   		<?php
		   		}
		   		
		   		if ( $events_id !== '' ) {
		   			?><li class="kunstraum events-page permalink-events"><a href="/expo-events/?eventid=<?php echo $current_ID; ?>">Events</a></li>
		   			<?php
		   			}
		   		
		   		 if ( $presse_id !== '' ) { 
		   		 ?><li class="kunstraum permalink-presse post-<?php echo $presse_id ; ?>"><a href="<?php echo $presse_url ; ?>">Presse</a></li>
		   		 <?php
		   		 }
		   		 
		   		 if ( $publikation_id !== '' ) { 
		   		 ?><li class="kunstraum permalink-publikation post-<?php echo $publikation_id ; ?>"><a href="<?php echo $publikation_url ; ?>">Publikation</a></li>
		   		 <?php
		   		 }
		   		 ?>
		   		
		    </ul><br/>
		    
		    </nav>
		    <div class="close-button">
			    <a href="#" title="Schliessen" class="unstyled">
			    <img src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>images/closing-cross.png" alt="" /></a>
		    </div>
		    
		    <?php 
		    // OPTIONAL SECOND MENU:
		    // if we have several TdC items, we need to build a second MENU....
		    if ( $tribune_nr > 1) {
		    // echo the first item
		   ?><div id="tdc-nav" class="hidden"> 
		   	<ul class="tdc-nav clean exhib-ajax-nav default-menu horiz-list small-font">
			   <li class="post-<?php echo $tribune_id; ?>">&mdash;&nbsp;&nbsp;<a href="<?php echo $tribune_url; ?>" data-postid="post-<?php echo $tribune_id; ?>"><?php echo $tribune_title; ?></a>
			   </li>
		    <?php
		    
		    // NOW : generate the menu
		    // source http://wordpress.org/support/topic/query-posts-then-put-into-array
		    global $connected_tdc;
		    while( $connected_tdc->have_posts() ) :
		    $connected_tdc->the_post();
		    ?>
		    	<li class="post-<?php the_ID(); ?>">&mdash;&nbsp;&nbsp;<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" data-postid="post-<?php the_ID(); ?>"><?php the_title(); ?></a>
		    	</li>
		    <?php endwhile; ?>
		    </ul>
		    </div><!-- .tdc-nav -->
		    <?php // Prevent weirdness
		    wp_reset_postdata();
		    }  // END OPTIONAL SECOND MENU
		    
		     ?>
		    </header>
		    <div id="exhib-ajax-container" class="exhib-ajax-container"></div>
	    
	    </div>
	    
	    <?php
	    // check for meta-fields
	    
	    $video_expo = get_post_meta($post->ID, 'Video-expo-clip', true);
	    $video_talk = get_post_meta($post->ID, 'Video-gespraech', true);
	    

	    if($video_expo !== '' && $video_talk !== '' ) {
	    
	    
	    // fix the URLs for every case
	    // we want ONLY the NUMBER...
	    $video_expo = str_replace("http://vimeo.com/", "", $video_expo);
	    $video_expo = str_replace("https://vimeo.com/", "", $video_expo);
	    $video_expo = rtrim($video_expo,"/");
	    $video_talk = str_replace("http://vimeo.com/", "", $video_talk);
	    $video_talk = str_replace("https://vimeo.com/", "", $video_talk);
	    $video_talk = rtrim($video_talk,"/");
	    
	    echo '<div class="expo-img-large vimeo-frame"><iframe src="https://player.vimeo.com/video/'. $video_expo .'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="496" height="374" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
	    
	    echo '<div class="expo-img-medium vimeo-frame"><iframe src="https://player.vimeo.com/video/'. $video_talk .'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="244" height="183" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
	    
	    expo_img_toolbox();
	    
	    }
	    
	   else if($video_expo !== '') { // has video-expo only
	   
		   $video_expo = str_replace("http://vimeo.com/", "", $video_expo);
		   $video_expo = str_replace("https://vimeo.com/", "", $video_expo);
		   $video_expo = rtrim($video_expo,"/");
	   
	    
	    		echo '<div class="expo-img-large vimeo-frame"><iframe src="https://player.vimeo.com/video/'. $video_expo .'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="496" height="374" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
	    		
		    	expo_img_toolbox();
		    
		}
		
		else if($video_talk !== '') { // has video-talk only
		 	
		 	$video_talk = str_replace("http://vimeo.com/", "", $video_talk);
		 	$video_talk = str_replace("https://vimeo.com/", "", $video_talk);
		 	$video_talk = rtrim($video_talk,"/");
		 	
		 		echo '<div class="expo-img-large vimeo-frame"><iframe src="https://player.vimeo.com/video/'. $video_talk .'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="496" height="374" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
		 		
			    	expo_img_toolbox();
			    
			}
			
		 else { // no video at all ? OK:
		    	
		    		first_img_toolbox();
		    		
		    		expo_img_toolbox(1); // number = offset = skips first image
		    		
		 } ?>
		
		<script>		
	jQuery(document).ready(function($){
	
		$("#main .expo-img-container img").removeAttr("title"); //	
	
		$("#main").on("click", ".img-fx-first", function () {
		
		var BigImgSrc = ($(this).children("a").attr("href")); //get big IMG
		var SmallImgSrc = ($(this).find("img").attr("src")); //get small IMG
		
		$(this).find("img").attr({
			  src: BigImgSrc,
			  width: '748', 
			  height: 'auto'
			});
			$(this).children("a").attr({rel: SmallImgSrc});
			$(this).find("img").css({'max-width': '748px'});
			$(this).css({width: '748px', height: 'auto'});
			
		$(this).removeClass('img-fx-first');
		$(this).addClass('img-expanded-first');
		return false;
		});
		
		$("#main").on("click", ".img-expanded-first", function(){
		// restore original image
			var RestoreSrc = ($(this).children("a").attr("rel"));
		 $(this).find("img").attr({
		 	src: RestoreSrc,
		 	width: '496'
		 });
		 $(this).find("img").css({'max-width': '496px'});			 
		 $(this).css({width: '496px', height: '374px'});		 
		 $(this).removeClass('img-expanded-first').addClass('img-fx-first');
		return false;
		});
		
		 
		 $("#main").on("click", ".img-fx-medium", function () {

		 	var BigImgSrc = ($(this).children("a").attr("href")); //get big IMG
		 	var SmallImgSrc = ($(this).find("img").attr("src")); //get small IMG
		 	
		 	$(this).find("img").attr({
		 	  src: BigImgSrc,
		 	  width: '748', 
		 	  height: 'auto'
		 	});
		 	$(this).children("a").attr({rel: SmallImgSrc});
		 	$(this).find("img").css({'max-width': '748px'});
		 	$(this).css({width: '748px', height: 'auto'});
		 	
		 			 	
			$(this).removeClass('img-fx-medium');
			$(this).addClass('img-expanded-m');
			//alert('done');
		 	return false;
		 	
		 });
		 
		 $("#main").on("click", ".img-expanded-m", function(){
		 //alert('OK!');
		 
		 // restore original image
		 	var RestoreSrc = ($(this).children("a").attr("rel"));
		  $(this).find("img").attr({
		  	src: RestoreSrc,
		  	width: '244'
		  });
		  $(this).find("img").css({'max-width': '244px'});			 
		  $(this).css({width: '244px', height: '183px'});		 
		  $(this).removeClass('img-expanded-m').addClass('img-fx-medium');
		 
		 return false;
		 });
			 						
	});
	
		</script>
		
	</article>
</div> <!-- .mainframe -->


