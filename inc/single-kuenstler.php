<!-- Template: single-kuenstler.php -->
 
  <article <?php post_class('mainframe') ?> id="post-<?php the_ID(); ?>">
  
	  <footer class="meta-inform zero link-u">
		  <ul>
			<li>Datum: <time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('F jS, Y') ?></time></li>
			<li>Kategorie: <?php the_category(' / ') ?></li>
		  </ul>
	  </footer>
	  
    <header>
    
      <h1 class="main-title h1"><?php the_title(); ?></h1>
            
    </header>
    
    <div class="full-block clearfix">
	    <div class="main-content">
	    	<?php the_content('Read the rest of this entry &raquo;'); ?>
	    </div>
	    
	    <?php
	      // Find connected pages - exhibitions
	      $connected = new WP_Query( array(
	        'connected_type' => 'posts_to_kuenstler',
	        'connected_items' => get_queried_object_id(),
	      ) );
	      
	      // Display connected pages
	      if ( $connected->have_posts() ) :
	      ?>
	      <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
	      	<h3>Ausstellung: <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	      <?php endwhile; ?>
	      
	      <?php 
	      // Prevent weirdness
	      wp_reset_postdata();
	      
	      endif; //connected pages
	      ?>
		
		<div class="right-block post-meta">
		
		<?php // check for meta fields : Title, Year, Type ...
				$ms_year = get_post_meta($post->ID, 'Year', true);
				$ms_type = get_post_meta($post->ID, 'Type', true);
		?>

							
		</div><!-- .right-block -->
    </div><!-- .full-block -->

  </article>


