<!-- Template: pages-expo-nav.php -->
   
<nav class="nav-expos clearfix small-font">
	<ul class="horiz-list default-menu">
		<li class="current-menu-item"><a href="<?php bloginfo('url'); ?>/ausstellungen/">Übersicht</a></li>
		<li class=""><a href="<?php bloginfo('url'); ?>/kuenstler/" id="a-kuenstler" class="a-kuenstler">Künstler A - Z</a></li>
	</ul>
	
	<div id="k-liste" class="k-liste hidden">Künstler A - Z</div>
	
	<ul class="horiz-list default-menu actual-exhib">
	<?php 
//	query_posts(array( /////// NEWEST
//		'posts_per_page' => 1,
//		'category' => 9,
//		'orderby' => 'post_date',
//		'order' => 'DESC'
//		)); 
//		
//		 if (have_posts()) : while (have_posts()) : the_post();
		 
		 $args = array( 
		 	  	//'category' => $search_category, 
		 	  	'category' => 9, 
		 	  	'numberposts' => 1, 
		 	  	'orderby' => 'post_date', 
		 	  	'order' => 'DESC', 
		 	  	); 
		 	  $myposts = get_posts($args);
		 foreach($myposts as $post) : 
		 // Setup the post variables
		 	  	setup_postdata($post);
		 
		  ?>
	
	<li><a href="<?php the_permalink(); ?>" title="<?php 
					$titleslug = basename(get_permalink()); 
					echo $titleslug; 
		  	  		 ?>">Aktuelle Ausstellung</a></li>
	
	<?php endforeach; 
		wp_reset_postdata(); ?>
	
		
	</ul>
	
</nav>