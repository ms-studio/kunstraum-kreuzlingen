<?php
/*
Template Name: Infobox
*/

//if($_SERVER['HTTP_X_REQUESTED_WITH']==''){
	get_header();
//} 
?>

<!-- Template: page-infobox.php -->

<div id="main" role="main">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  
  <article <?php post_class('infobox-content darker-grey') ?> id="post-<?php the_ID(); ?>">
    <header class="header">
      	<?php wp_nav_menu( array(
      		 'theme_location' => 'info-menu',
      		 'container'       => 'nav',
      		 'container_class'       => 'clearfix default-menu horiz-list small-font',
      		 'depth'           => 0,
      		 //'link_after'       => '&nbsp;',
      		 ) ); ?>
      		 
      	<div class="close-button hidden unstyled"><a href="#" title="Schliessen"><img src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>images/closing-cross.png" alt="" /></a></div>	 
    </header>
  
  <div class="full-block clearfix content">
	    <div class="main-content small-font">
  		  <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
  		  </div>
  </div>
  
  <?php edit_post_link('bearbeiten', '<p class="edit">[ ', ' ]</p>'); ?>
     
   <script type="text/javascript">
   $.fn.emailSpamProtection = function(className) {
   	return $(this).find("." + className).each(function() {
   		var $this = $(this);
   		var s = $this.text().replace(" [at] ", "&#64;");
   		$this.html("<a href=\"mailto:" + s + "\">" + s + "</a>");
   	});
   };
   
   $("body").emailSpamProtection("email");
   </script>
  
  </article>
  <?php endwhile; endif; ?>

</div>

<?php 
//if($_SERVER['HTTP_X_REQUESTED_WITH']==''){
	get_footer(); 
//} 
?>
