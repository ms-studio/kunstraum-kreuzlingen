jQuery(document).ready(function($){

/*
 * CONTENTS
 *
 *   1: Email Spam Protection 
 *   2: AJAX Loaders for Info-box
 *   3: AJAX Loaders for Exhibitions
 *   4: artist list AJAX
 *   5: CLOSE BUTTON
 *   6: sortieren-list
 *
 */
 
 
/* 
 * 0: js-hidden must be hidden
 ****************************************************
 */ 
 $(".js-hidden").hide();

/* 
 * 1.
 * EmailSpamProtection (jQuery Plugin)
 ****************************************************
 * Author: Mike Unckel
 * Description and Demo: http://unckel.de/labs/jquery-plugin-email-spam-protection
 * Example: <span class="email">info [at] domain.com</span>
 */
$.fn.emailSpamProtection = function(className) {
	return $(this).find("." + className).each(function() {
		var $this = $(this);
		var s = $this.text().replace(" [at] ", "&#64;");
		$this.html("<a href=\"mailto:" + s + "\">" + s + "</a>");
	});
};
$("body").emailSpamProtection("email");

/* 
 * 2: AJAX - Defaults for Listing Page
 ****************************************************
 */
 
 var contbox = $('.exhib-ajax-listing'); // define container element
 var container = $('.exhib-ajax-container-listing');
 
$.ajaxSetup({
	timeout:5000,
	error: function(req,error){
	  if(error === 'error'){error = req.statusText;}
	  var errormsg = 'There was a communication error: '+error;
	  container.html('<div class="ajax-error font-norm">'+errormsg+'</div>').effect('highlight',{color:'#c00'},1000);
	}
});

/* 
 * 2: AJAX - for Listing Pages (synopsen etc)
 ****************************************************
 */
 
 /*
 * replaced with the .load method
 */
 
 $('.list-item .ajaxtrigger').click(function(){ // info
  	var toLoad = $(this).attr('href')+' #main';
  	// we must also get the attribute data-location
  	var locstyle = $(this).data('location');
  	// ... and assign it to #exhib-ajax
  	$('.exhib-ajax-listing').removeClass("style-tiefparterre");
  	$('.exhib-ajax-listing').addClass(locstyle);
  	// while loading:
  	$('.exhib-ajax-listing').show('fast');
  	$('.exhib-ajax-container-listing').html('<div class="ajax-loading"></div>');
  	// now the AJAX thing:
 	$('.exhib-ajax-container-listing').load(toLoad, function() {
 		$('.exhib-ajax-container-listing').emailSpamProtection("email");
 		});
 	return false;
 });


 
/* 
 * 2: AJAX - play vimeo
 ****************************************************
 */ 	 
 
 $('.ajaxtrigger-vimeo').click(function(){ // the trigger action 
   //playVimeo($(this).data('vimeo'));
   // ... we must also get the attribute data-location
   var vimeokey = $(this).data('vimeo');
   var locstyle = $(this).data('location');
   // ... and assign it to #exhib-ajax
   // contbox.removeClass("style-tiefparterre");
   // contbox.addClass(locstyle);
   // contbox.addClass("vimeo-container");
   // 240 x 180px
   $(this).replaceWith('<iframe src="https://player.vimeo.com/video/' + vimeokey + '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1" width="240" height="180" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen class="list-item-inside dblock"></iframe>');
   // $('.close-button').hide();
   return false;
 });
 

/* 
 * 2: AJAX loaders for Info-Box
 ****************************************************
 */
 
$('#menu-item-59').click(function(){ // info
 	var toLoad = $(this).children("a").attr('href')+' #main';
 	$('#infobox').html('<div class="ajax-loading"></div>');
	$('#infobox').load('/information/wo/ #main', function() {
		$('#infobox').removeClass('infobox-home');
		$('#infobox').show('fast');
		$("#infobox").emailSpamProtection("email");
		});
	return false;
}); // end of first part

// now the navigation menu	
$("#infobox").on("click", "nav li", function(){
	var toLoad = $(this).children("a").attr('href')+' #main';
	$('#infobox .content').html('<div class="ajax-loading"></div>');
	$('#infobox').load(toLoad, function() {
		$('#infobox').show('fast');
		$("#infobox").emailSpamProtection("email");
		});
		return false;
}); // end of second AJAX part


$("#infobox.infobox-home").on("click", "a.ajax", function(){
		var toLoad = $(this).attr('href')+' #main';
		$('#infobox').load(toLoad, function() {
			$('#infobox').removeClass('infobox-home');
			$("#infobox").emailSpamProtection("email");
		});
	return false;
}); // end of third AJAX part

// CLOSE BUTTON of Info-Box
$("#infobox").on("click", ".close-button", function(){
			$('#infobox').hide('fast');
			return false;
}); // close button	

/* 
 * 3: AJAX loaders for EXHIBITION page
 ****************************************************
 */

$("#connected-material").on("click", "a", function(){
	var activItm = $(this).attr('id');
	var toLoad = $(this).attr('href')+' #mainframe';
	$('#exhib-ajax-container').html('<div class="ajax-loading"></div>');
		$('#exhib-ajax-nav li').removeClass('current-menu-item');
		$('#exhib-ajax-nav li.'+activItm).addClass('current-menu-item');
		$('#exhib-block-m').hide('fast');
		$('.expo-img-large').hide('fast');
		$("#exhib-ajax .close-button").show('fast');
		$('#exhib-ajax').fadeIn(500);
	//alert(activItm);		 				 		
	$('#exhib-ajax-container').load(toLoad, function() {
				$("#exhib-ajax-container").emailSpamProtection("email");
				
		});
return false;
}); // end EXHIBITION AJAX part 1
	
// exhib-ajax-nav
$(".exhib-ajax-nav").on("click", "a", function(){
	var toLoad = $(this).attr('href')+' #main';	
	$('#exhib-ajax-container').html('<div class="ajax-loading"></div>');	 				 		
	$('#exhib-ajax-container').load(toLoad, function() {
			$('.exhib-ajax-nav li').removeClass('current-menu-item');
			// $('#tdc-nav').hide('fast');
			var activItm = $('#exhib-ajax-container #mainframe').attr('data-postid');
			// alert(activItm);
			$('.exhib-ajax-nav li.'+activItm).addClass('current-menu-item');
			$("#exhib-ajax-container").emailSpamProtection("email");
		});
		return false;
}); // end of second AJAX part

// some SHOW / HIDE for Tribune-de-Critique Sub.Menu

$(".permalink-tribune").click(function(){
	$('#tdc-nav').fadeIn(500);
});

$("#tdc-nav ul").on("click", "a", function(){
	var thisItm = $(this).attr('data-postid');
	// alert(thisItm);
	$('#exhib-ajax-nav li.permalink-tribune').addClass(thisItm);
});

$(".permalink-synopse, .permalink-events, .permalink-presse, .permalink-rede, .permalink-publikation").click(function(){
	$('#tdc-nav').hide('fast');
});
	
/* 
 * 4: artist list AJAX
 ****************************************************
 * NOW DONE IN PHP / CSS
 */
	

	
/* 
 * 5: CLOSE BUTTON
 ****************************************************
 */

		$("#exhib-ajax").on("click", ".close-button", function(){
				$('#exhib-ajax').hide('fast');
				$('#tdc-nav').hide('fast');
				// we should also EMPTY the #exhib-ajax-container
				$('#exhib-ajax-container').html('<p></p>');
				$('#exhib-block-m').show('fast');
				$('.expo-img-large').show('fast');
						  // alert('OK');
						  return false;
		}); // close button

/* 
 * 6: Sortieren-List in Header
 ****************************************************
 */
		
	$("#sortieren").on("mouseenter", function(){
		// $('#sortieren-ul').show();
	}); // 
	
	$(".sortieren-nach").on("click", function(){
		// $('#sortieren-ul').show();
		return false;
	}); 
	
	$('#sortieren').on("mouseleave", function(){
		// $('#sortieren-ul').hide();
	});

/* 
 * 6: Show / Hide items
 ****************************************************
 */

	$("#show-hide-items li").click(function(){
		// reset underlines
		$("#show-hide-items li").removeClass("current-menu-item");
		$(this).addClass("current-menu-item");
		// reset the grid system !!
		return false;
	 });
	
	$("#show-kunstraum").click(function(){
		
		$(".list-item-kunstraum").show(500);
		$(".list-item-tiefparterre").hide(500);
		return false;
	 });
	 
 	$("#show-tiefparterre").click(function(){
 	 	
	 	 $(".list-item-tiefparterre").show(500);
	 	 $(".list-item-kunstraum").hide(500);
	 	 return false;
 	 });
 	  
 	$("#show-both").click(function(){
 	 
	 	 $(".list-item-tiefparterre").show(500);
	 	 $(".list-item-kunstraum").show(500);
	 	 return false;
 	 });
 	 

/* 
 * that's it !
 ****************************************************
 */
 				
}); // end document ready
		
		
		