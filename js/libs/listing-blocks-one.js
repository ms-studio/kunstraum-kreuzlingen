$(document).ready(function() {	

	 // STEP 1:
 	 //
 	 // align the modules in a grid

	$(".item-4").addClass("grid-item-four");
	$(".item-4plus").addClass("grid-item-four-plus");

	$("#show-hide-items li").click(function(){
		// reset underlines
		$("#show-hide-items li").removeClass("current-menu-item");
		$(this).addClass("current-menu-item");
		// reset the grid system !!
		 $(".grid-item-four").removeClass("grid-item-four");
		 $(".grid-item-four-plus").removeClass("grid-item-four-plus");
		return false;
	 });
	
	$("#show-kunstraum").click(function(){

		$(".itemkr-4").addClass("grid-item-four");
		$(".itemkr-4plus").addClass("grid-item-four-plus");
		
		$(".list-item-kunstraum").show(500);
		$(".list-item-tiefparterre").hide(500);
		return false;
	 });
	 
 	$("#show-tiefparterre").click(function(){

		$(".itemtp-4").addClass("grid-item-four");
		$(".itemtp-4plus").addClass("grid-item-four-plus");
 	 	
	 	 $(".list-item-tiefparterre").show(500);
	 	 $(".list-item-kunstraum").hide(500);
	 	 return false;
 	 });
 	  
 	$("#show-both").click(function(){

		 $(".item-4").addClass("grid-item-four");
		 $(".item-4plus").addClass("grid-item-four-plus");
 	 
	 	 $(".list-item-tiefparterre").show(500);
	 	 $(".list-item-kunstraum").show(500);
	 	 return false;
 	 });
 	 
 }); // end document.ready