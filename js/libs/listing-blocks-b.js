$(document).ready(function() {	

 	 //
 	 // align the modules in a grid
 	 // very simplified version...



	$("#show-hide-items li").click(function(){
		// reset underlines
		$("#show-hide-items li").removeClass("current-menu-item");
		$(this).addClass("current-menu-item");
		// reset the grid system !!

		return false;
	 });
	
	$("#show-kunstraum").click(function(){
		
		$(".list-item-kunstraum").show(500);
		$(".list-item-tiefparterre").hide(500);
		return false;
	 });
	 
 	$("#show-tiefparterre").click(function(){
 	 	
	 	 $(".list-item-tiefparterre").show(500);
	 	 $(".list-item-kunstraum").hide(500);
	 	 return false;
 	 });
 	  
 	$("#show-both").click(function(){
 	 
	 	 $(".list-item-tiefparterre").show(500);
	 	 $(".list-item-kunstraum").show(500);
	 	 return false;
 	 });
 	 
 }); // end document.ready