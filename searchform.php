<?php
/**
 * The template for displaying search forms
 *
 * @package WordPress
 * @subpackage Kunstraum Kreuzlingen
 */
?>

<form method="get" id="searchform" class="aside-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="s" class="assistive-text hidden">Suche</label>
	<input type="text" class="field" name="s" id="s" placeholder="Suche" />
	<input type="submit" class="submit hidden" name="submit" id="searchsubmit" value="Suche" />
</form>
