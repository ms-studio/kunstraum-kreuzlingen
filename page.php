<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */

get_header(); ?>

<!-- Template: page.php -->

<div id="main" role="main">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  
  <article <?php post_class('mainframe') ?> id="post-<?php the_ID(); ?>">
    <header>
      <h1 class="main-title"><?php the_title(); ?></h1>
    </header>
  
  <div class="full-block clearfix">
	    <div class="main-content">
  		  <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
  		  </div>
  		  
  		  <?php
  		  
  		  if ( 'kuenstler' == get_post_type() ) {
  		  
  		  // Find connected pages
  		  $connected = new WP_Query( array(
  		    'connected_type' => 'posts_to_kuenstler',
  		    'connected_items' => get_queried_object_id(),
  		  ) );
  		  
	  		  // Display connected pages
	  		  if ( $connected->have_posts() ) :
	  		  ?>
	  		  <h3>Related pages:</h3>
	  		  <ul>
	  		  <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
	  		  	<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	  		  <?php endwhile; ?>
	  		  </ul>
	  		  
	  		  <?php 
	  		  // Prevent weirdness
	  		  wp_reset_postdata();
	  		  
	  		  endif; //connected pages
  		  
  		  } //
  		  ?>

   </div>
  
  </article>
  <?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
