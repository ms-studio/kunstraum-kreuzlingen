<?php
/*
Template Name: Listing-Blocks
*/

get_header(); ?>

<!-- page-listing-blocks -->

<div id="main" role="main" class="page-listing page-listing-blocks">
  <?php if (have_posts()) : while (have_posts()) : the_post(); 
  
  /******************** 
   * NOTES :
   * the method that we use to display all the items
  
     * 1. We LOOP all the Exhibitions
     * 2. We test for a Connected Artist
     * 3. For each exhibition, we query for attached materials
  
   *******************/
   
  // 0. Pre-Processing:
  // We define a variable for the content type (term) we are looking for:
  // synopse ? - or tribune de critique ?
  
  if ( $post->post_name == 'tribune-de-critique' ) {
  
  	$current_term = 'tribune-de-critique';
  	$kkloop_mode = 'term';
  
  } else if ( $post->post_name == 'expo-clip' ) {
  
  	// $kk_vimeo_key = get_post_meta($post->ID, 'Video-expo-clip', true);
  	$current_meta = 'Video-expo-clip';
  	$kkloop_mode = 'meta';
  	$current_term = 'synopsen';

  } else if ( $post->post_name == 'gespraeche-tv' ) {
  
  	$current_meta = 'Video-gespraech';
  	// $kk_vimeo_key = get_post_meta($post->ID, 'Video-expo-clip', true);
  	$kkloop_mode = 'meta';
  	$current_term = 'synopsen';

  } else if ( $post->post_name == 'reden' ) {
  
  	$current_term = 'reden';
  	$kkloop_mode = 'term';
  
  } else {
  
  	$current_term = 'synopsen';
  	$kkloop_mode = 'term';
  	// this will be our fallback term
  }
  
  // kkloop_mode will define the sort of query we run...
  // for taxonomy terms (synopse, tribune...)
  // or for meta fields (videos)
  
  ?>
  <article id="post-<?php the_ID(); ?>" class="mainframe">
    <header class="header clearfix">
      <h1 class="main-title listing-main-title leftfloat h1"><?php the_title(); ?></h1>
      
      <nav class="nav-expos nav-listing clearfix small-font">
	      <ul id="show-hide-items" class="horiz-list default-menu">
		      <li id="show-both" class="current-menu-item"><a href="#">Beide</a></li>
		      <li id="show-kunstraum"><a href="#">Kunstraum</a></li>
		      <li id="show-tiefparterre"><a href="#">Tiefparterre</a></li>
	      </ul>
      </nav>
      
    </header>
   <?php endwhile; endif; ?>
  	
  <div id="list-container" class="superwide clearfix">
  
  	<div id="exhib-ajax" class="exhib-ajax exhib-ajax-listing exhib-block-m hidden">
	  		<div class="close-button">
	  		    <a href="#" title="Schliessen" class="unstyled">
	  		    <img src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>images/closing-cross.png" alt="" /></a>
	  		</div>
  		<div id="exhib-ajax-container" class="exhib-ajax-container exhib-ajax-container-listing"></div>
  	</div>
  
  	<?php // global $post;
  	
  			$itemloop = 0; // variable for counting rows
  			$itemloopkr = 0; // variable for kunstraum
  			$itemlooptp = 0; // variable for tiefparterre
  			
  			
  			/***************************************** 
  			 	* LOOP Nr 1.
  				* We query for all the Exhibitions
  			 *****************************************/
  	
  			$list_expos = new WP_Query(array(
  				'posts_per_page' => -1,
  				'category' => array(3, 4), // Kunstraum + Tiefparterre
  				'orderby' => 'post_date',
  				'order' => 'DESC',
  				'supress_filters' => false
  				)); 
  				  				
  			while( $list_expos->have_posts() ) : $list_expos->the_post();
  			 
  			 	$this_post_id = get_the_ID();

  				// reset the variables
  				$exhib_loc = '';
  				$exhib_status = '';
  				$kk_artist_name = '';
  				$kk_artist_url = '#';
  				
  				if ( in_category( 'kunstraum' ) ) {
  				  	$exhib_loc = 'kunstraum';
  				} else {
  					// we are in tiefparterre
  					$exhib_loc = 'tiefparterre';
  				} // end of first test
  				
  				if ( in_category( 'vorschau' ) ) {
  				// set variable
  					$exhib_status = 'vorschau';
  				} elseif ( in_category( 'aktuell' ) ) {
  					$exhib_status = 'aktuell';
  				// set variable
  				} else {
  					$exhib_status = '';
  				}// end of second test
  				
  				$exhib_url = get_permalink();
  				$exhib_title = get_the_title();
  				
  				// 
  				
  				if ( $kkloop_mode == 'meta') {
  				
  					$kk_vimeo_key = get_post_meta($post->ID, $current_meta, true);
  					// http://codex.wordpress.org/Function_Reference/get_post_meta
  				}
  				// $kk_vimeo_key = get_post_meta($post->ID, 'Video-expo-clip', true);
  				
  				$kk_artist_name = get_post_meta($post->ID, 'Künstler-Name', true);
  				
  				
  				// NOTE: if there is a Künstler-Name meta tag set, we will use that.
  				// Otherwise, we will query for a connected artist.
  				
  				/***************************************** 
  				 	* LOOP Nr 2.
  					* Find connected material - Artist
  				 *****************************************/
  					
  				$connected_artist = new WP_Query( array(
  					'posts_per_page' => 1,
  				  'connected_type' => 'posts_to_kuenstler',
  				  'connected_items' => $this_post_id,
  				  'post_type' => 'kuenstler',
  				) );
  				
  				if ( $connected_artist->have_posts() ) :
  				while ( $connected_artist->have_posts() ) : $connected_artist->the_post(); 
  				
  				// set variable
  				$kk_artist_name = get_the_title();
  				// $kk_artist_url = get_permalink(); NOT NEEDED
  				
  				endwhile; 
  					wp_reset_postdata();
  				else :
  				
  				endif; // END connected ARTIST query
  				
  				/***************************************** 
  				 	* LOOP Nr 3.
  					* is split in sub-templates
  				 *****************************************/
  				 
  				
  				if ( $kkloop_mode == 'term') {
  					 include( TEMPLATEPATH . '/inc/listing-loop-term.php' );
  				}
  				else { // $kkloop_mode == 'meta'
  				
  					include( TEMPLATEPATH . '/inc/listing-loop-meta.php' );
  					 
  				}
  				
  				/***
  				 *****************************************/
  				
  			endwhile; 
  				// end of LOOP Nr 1.
  			?>
  				
  	</div> <!-- .superwide -->
  	
  	</article>
  
</div> <!-- #main -->


<?php get_footer(); ?>
