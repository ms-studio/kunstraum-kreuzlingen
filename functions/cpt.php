<?php

add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts() {

	$labels = array(
		"name" => "Künstler",
		"singular_name" => "Künstler",
		"menu_name" => "Künstler",
		"add_new" => "Künstler erstellen",
		"edit" => "Bearbeiten",
		"edit_item" => "Künstler bearbeiten",
		"new_item" => "Neuer Künstler",
		"view_item" => "Künstler ansehen",
		"search_items" => "Künstler suchen",
		);

	$args = array(
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => true,
		"query_var" => true,
		"menu_position" => 7,		
		"supports" => array( "title", "editor", "excerpt", "custom-fields", "revisions", "thumbnail" ),		
	);
	register_post_type( "kuenstler", $args );

	$labels = array(
		"name" => "Materialien",
		"singular_name" => "Material",
		"menu_name" => "Materialien",
		"add_new" => "Material hinzufügen",
		"add_new_item" => "Material hinzufügen",
		"edit" => "Bearbeiten",
		"edit_item" => "Material bearbeiten",
		"new_item" => "Material hinzufügen",
		"view" => "Ansehen",
		"view_item" => "Material ansehen",
		"search_items" => "Suchen",
		"not_found" => "Kein Resultat",
		"not_found_in_trash" => "Kein Resultat",
		"parent" => "Übergeordnetes Material",
		);

	$args = array(
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "material", "with_front" => false ),
		"query_var" => true,
		"menu_position" => 6,		
		"supports" => array( "title", "editor", "custom-fields", "revisions", "thumbnail" ),		
	);
	register_post_type( "kk_material", $args );

// End of cptui_register_my_cpts()
}


add_action( 'init', 'cptui_register_my_taxes' );
function cptui_register_my_taxes() {

	$labels = array(
		"name" => "Kategorien",
		"label" => "Kategorien",
		"search_items" => "Suchen",
		"popular_items" => "Häufig genutzt",
		"all_items" => "Alle Kategorien",
		"parent_item" => "Übergeordnete Kategorie",
		"parent_item_colon" => "Übergeordnete Kategorie",
		"edit_item" => "Kategorie bearbeiten",
		"update_item" => "Kategorie speichern",
		"add_new_item" => "Neue Kategorie hinzufügen",
		"new_item_name" => "Neue Kategorie",
		);

	$args = array(
		"labels" => $labels,
		"hierarchical" => 1,
		"label" => "Kategorien",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'material_types', 'with_front' => false ),
		"show_admin_column" => true,
	);
	register_taxonomy( "material_types", array( "kk_material" ), $args );

// End cptui_register_my_taxes
}
