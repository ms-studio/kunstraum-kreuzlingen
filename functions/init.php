<?php

// Initialize Theme

// Change-Detector-XXXXXXXXXXXXXXX - for Espresso.app

/* Allow Automatic Updates
 ******************************
 * http://codex.wordpress.org/Configuring_Automatic_Background_Updates
 */

add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
add_filter( 'allow_major_auto_core_updates', '__return_true' );


/* 1. Register CSS and JS
**************/


function custom_register_styles() {

	/**
	 * Custom CSS
	 */
	 
	 $kk_dev_mode = KK_DEV_MODE; // dev or prod
	 
	 if ( $kk_dev_mode == 'dev' ) {
	 
	 		// DEV: the MAIN stylesheet - uncompressed
	 		wp_enqueue_style( 
	 				'main_css_style', 
	 				get_stylesheet_directory_uri() . '/css/dev/00-main.css?ver=13', // main.css
	 				false, // dependencies
	 				null // version
	 		); 
	 
	 } else {
	 
	 		// PROD: the MAIN stylesheet - combined and minified
	 		wp_enqueue_style( 
	 				'main_css_style', 
	 				get_stylesheet_directory_uri() . '/css/build/styles.20230404A.css', // main.css
	 				false, // dependencies
	 				null // version
	 		); 
	 }

	wp_enqueue_style( 
			'fonts_css', 
			'https://fonts.googleapis.com/css?family=Open+Sans:400,700',
			false,
			null
	); 

	/**
	 * Custom JavaScript
	 */

	wp_enqueue_script(
			'modernizer_js', // handle
			get_stylesheet_directory_uri() . '/js/libs/modernizr.custom.14446.min.js', //src
			false, // dependencies
			null, // version
			false // in_footer
	);
	
	wp_register_script(
			'jquery',
			get_site_url() . '/wp-includes/js/jquery/jquery.js',
			false, // dep
			'1.10.2', // jquery version
			false // load in footer?
	);
	wp_enqueue_script( 'jquery' );


	if ( $kk_dev_mode == 'dev' ) {
	
			wp_enqueue_script( 
			// our MAIN custom JavaScript file 
					'main-js', // handle
					get_stylesheet_directory_uri() . '/js/script.js', // scripts.js
					array('jquery'), // dependencies
					'11.07.15', // version
					true // in_footer
			);
	
	} else {
	
			// PROD: the MAIN stylesheet - combined and minified
			wp_enqueue_script( 
			// our MAIN custom JavaScript file 
					'main-js', // handle
					get_stylesheet_directory_uri() . '/js/build/scripts.20211027165257.js',
					array('jquery'), // dependencies
					null, // version
					true // in_footer
			);
	}

}

add_action( 'wp_enqueue_scripts', 'custom_register_styles', 10 );


