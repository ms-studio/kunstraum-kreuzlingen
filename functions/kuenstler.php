<?php

// Build Artists Menu:

function kk_build_artist_menu() {
  
  // check for transient.
  if ( is_user_logged_in() || ( false === ($kk_artist_menu = get_transient('kk_artist_menu')) ) ) {
  		
  		$kk_artist_menu = '<ul id="ul-kuenstler" class="ul-kuenstler clean unstyled hoverable">
  		<div class="ul-k-inside">';
  		
        $the_query = new WP_Query(array(
        'posts_per_page' => -1,
        'post_type' => 'kuenstler',
        'orderby' => 'title',
        'order'    => 'ASC',
      ));
       
      // What we do here: just display a list of all existing ARTISTS
      
      // p2p_type( 'posts_to_kuenstler' )->each_connected( $wp_query );
      // p2p_type is not flexible enough for what we want to do...
      
      if ( $the_query->have_posts() ) {
      
      	while ( $the_query->have_posts() ) {
      		
      		$the_query->the_post();
      		
      		$this_post_id = get_the_ID();
      		$kk1_artist_name = get_the_title();
      		
      		// OBJECTIVE : get the connected EXHIBITIONS
      		// (because we need their URL)
      		// issue #1 : some artists may not have an EXHIBITION attached.
      		// NOW we FIRST check if any EXHIBITION is attached...
      		
      			$connected_artist = new WP_Query( array(
      				'posts_per_page' => 1, // we want only one link (the newest)
      			  'connected_type' => 'posts_to_kuenstler',
      			  'connected_items' => $this_post_id,
      			) );
      			
      			if ( $connected_artist->have_posts() ) {
      			
      				while ( $connected_artist->have_posts() ) { 
      				
      					$connected_artist->the_post(); 
      					
      					$kk1_expo_url = get_permalink(); 
      					
      					// now the output...
      					$kk_artist_menu .= '<li class="li-kuenstler">
      					<a class="a" href="'.$kk1_expo_url.'">'; 
      					$kk_artist_menu .=  $kk1_artist_name.'</a></li>';
      					
      				}
      				
      			} else {
      			
      			// no exhibition attached!
      			// reset artist name to none ...
      			// just for safety ...
      			
      			$kk1_artist_name = '';
      			$kk1_expo_url = '#';
      			
      			}
      			
      	} // end while $the_query
      	
      	wp_reset_postdata();
      	
      } // end if $the_query
      
      $kk_artist_menu .= '</div></ul>';
       
      // all done - register the transient. 
      set_transient('kk_artist_menu', $kk_artist_menu, 12 ); // * HOUR_IN_SECONDS  
  }
 
  return $kk_artist_menu;
  
}


	  

  // 
 
