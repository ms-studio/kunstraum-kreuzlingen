<?php
/*
Template Name: Listing-Events
*/

get_header(); ?>

<!-- page-listing-events -->

<div id="main" role="main" class="page-listing page-listing-events">
  <?php if (have_posts()) : while (have_posts()) : the_post(); 
  
  /******************** 
   * NOTES :
   * the method that we use to display all the items
  
     * 1. We LOOP all the Exhibitions
     	
     	note: i'ts possible that an exhibition has NO events
     	therefore we store NAME and HYPERLINK as variables.
     
     * 2. We test for a Connected ARTIST
     
     * 3. We test for the LINKED Exhibition
     
     * 4. For each exhibition, we query for attached MATERIALS
  
   *******************/
   
  // We define a variable for the content type (term) we are looking for:
  // synopse ? - or tribune de critique ?
  
  if ( $post->post_name == 'presse' ) {
  	$current_term = 'presse';
  } else if ( $post->post_name == 'publikationen' ) {
  	$current_term = 'publikationen';
  } else if ( $post->post_name == 'tribune-de-critique' ) {
  		$current_term = 'tribune-de-critique';
  } else {
  	$current_term = 'events';
  	// this will be our fallback term
  }
  
  ?>
  <article id="post-<?php the_ID(); ?>" class="mainframe">
    <header class="header clearfix">
      <h1 class="main-title listing-main-title leftfloat h1"><?php the_title(); ?></h1>
      
      <nav class="nav-expos nav-listing clearfix small-font">
	      <ul id="show-hide-items" class="horiz-list default-menu">
		      <li id="show-both" class="current-menu-item"><a href="#">Beide</a></li>
		      <li id="show-kunstraum"><a href="#">Kunstraum</a></li>
		      <li id="show-tiefparterre"><a href="#">Tiefparterre</a></li>
	      </ul>
      </nav>
      
    </header>
   <?php endwhile; endif; ?>
  	
  <div id="list-container" class="superwide clearfix">
  
  	<div id="exhib-ajax" class="exhib-ajax exhib-block-m hidden">
	  		<div class="close-button">
	  		    <a href="#" title="Schliessen" class="unstyled">
	  		    <img src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>images/closing-cross.png" alt="" /></a>
	  		</div>
  		<div id="exhib-ajax-container" class="exhib-ajax-container"></div>
  	</div>
  
  	<?php // global $post;
  	
  			
  			/***************************************** 
  			 	* LOOP Nr 1.
  				* We query for all the Exhibitions
  			 *****************************************/
  	
  			$list_expos = new WP_Query(array(
  				'posts_per_page' => -1,
  				'category' => array(3, 4), // Kunstraum + Tiefparterre
  				'orderby' => 'post_date',
  				'order' => 'DESC', // DESC = newest first
  				'supress_filters' => false
  				)); 
  				  				
  				// p2p_type( 'posts_to_kuenstler' )->each_connected( $list_expos, array(), 'artists' );
  				// p2p_type( 'materials_to_posts' )->each_connected( $list_expos, array(), 'materialz' );
  				
  			while( $list_expos->have_posts() ) : $list_expos->the_post();
  			 
  			 $this_post_id = get_the_ID();
  			  			
  			?>
  	  			  	  				
  				<?php
  				
  				// reset the variables
  				
  				$exhib_loc = '';
  				$exhib_status = '';
  				
  				if ( in_category( 'kunstraum' ) ) {
  				  	$exhib_loc = 'kunstraum';
  				} else {
  					// we are in tiefparterre
  					$exhib_loc = 'tiefparterre';
  				} // ok, now another test
  				
  				if ( in_category( 'vorschau' ) ) {
  				// set variable
  					$exhib_status = 'vorschau';
  				} elseif ( in_category( 'aktuell' ) ) {
  					$exhib_status = 'aktuell';
  				// set variable
  				} else {
  					$exhib_status = '';
  				}// end of test
  				
  				$exhib_url = get_permalink();
  				$exhib_title = get_the_title();
  				
  				$kk_artist_name = get_post_meta($post->ID, 'Künstler-Name', true);
  				
  				// NOTE: if there is a Künstler-Name meta tag set, we will use that.
  				// Otherwise, we will query for a connected artist.
  				
  				?>
  				
  				<?php
  				
  				/***************************************** 
  				 	* LOOP Nr 2.
  					* Find connected material - ARTIST
  				 *****************************************/
  					
  				$connected_artist = new WP_Query( array(
  					'posts_per_page' => 1,
  				  'connected_type' => 'posts_to_kuenstler',
  				  'connected_items' => $this_post_id,
  				  'post_type' => 'kuenstler',
  				) );
  				
  				if ( $connected_artist->have_posts() ) :
  				
  				while ( $connected_artist->have_posts() ) : $connected_artist->the_post(); 
  				
  				// set variable
  				
  				$kk_artist_name = get_the_title();
  				// $kk_artist_url = get_permalink(); NOT NEEDED
  				
  				endwhile; 
  				// wp_reset_postdata();
  				
  				else :
  				
  				// reset artist name to none ...
  				
  				// $kk_artist_name = '';
  				// $kk_artist_url = '#';
  				
  				 ?>
  				<?php 
  				endif; // END connected ARTIST query
  				?>
  				
  				<?php
  				
  				/***************************************** 
  				 	* LOOP Nr 3.
  					* We look for attached materials
  					* Synopse or other...
  				 *****************************************/
  					
  				$connected_syn = new WP_Query( array(
  				  'posts_per_page' => -1, // show all
  				  'connected_type' => 'materials_to_posts',
  				  'connected_items' => $this_post_id, // get_queried_object_id(),
  				  'post_type' => 'kk_material',
  				  'post_status' => array ('publish', 'future'),	    
  				  'orderby' => 'date',
  				  'order' => 'DESC', // DESC = newest first
  				  'tax_query' => array(
  				  		array(
  				  			'taxonomy' => 'material_types',
  				  			'field' => 'slug',
  				  			'terms' => $current_term,
  				  			// options: synopse, tribune de critique...
  				  			//'operator' => 'NOT IN'
  				  		)
  				  	),
  				) );
  				
  				// Display connected pages
  				
  				if ( $connected_syn->have_posts() ) :
  				
  				// YES, we have some MATERIALS ...
  				
  				/* 
  				 **********************************
  				 *
  				 * NOW we output generic content 
  				 * regarding the EXHIBITION
  				 * 
  				 ***********************************
  				*/
  				  				
  				?>
  				
  				<div class="list-events-main-box list-item-<?php 

  							echo $exhib_loc; ?>">
  				
	  				<div class="list-events-title small-font">
	  					<p><?php 
	  					if ($exhib_status == 'vorschau') {
	  					echo '<span class="expo-vorschau-prefix prefix">vorschau</span> ';
	  					} elseif ($exhib_status == 'aktuell') {
	  					echo '<span class="expo-aktuell-prefix prefix">aktuell</span> ';
	  					}
	  					echo $kk_artist_name; ?> 
	  					<a href="<?php echo $exhib_url; ?>" class="exhib-link"><?php 
	  						echo $exhib_title; 
	  					?></a></p>
	  				</div><!-- .list-item-title -->
	  				<div class="list-item-box">
  				
  				<?php 
  				
  				while ( $connected_syn->have_posts() ) : $connected_syn->the_post(); 
  				// test variables
  				
  				  				
  				$synopse_id = get_the_ID();
  				$synopse_url = get_permalink();
  				?>
  				
  					<div class="list-item">
  				  					
	  					<div id="post-<?php the_ID(); ?>" class="list-item-inside dblock large-font" data-location="style-<?php echo $exhib_loc; ?>">
	  							<?php 
	  										// specific output according to content type
	  										
							if ( has_term( 'events', 'material_types' ) ) {
							
								?><h3 class="bold-title"><?php the_title(); ?></h3>
								<div class="compact-content"><?php the_excerpt(); ?></div>
								<?php  	
								
							 } else if ( has_term( 'publikationen', 'material_types' ) ) {
							 
								?><h3 class="italic-title"><?php the_title(); ?></h3>
								<div class="compact-content"><?php the_content(); ?></div>
								<?php  	 
									
							 } else if ( has_term( 'presse', 'material_types' ) ) {
							 
							 		kk_get_pdf();
									
							 } else {

								?><h3 class="bold-title"><?php the_title(); ?></h3>
								<div class="compact-content"><?php 
								
								the_excerpt(); 
								kk_get_pdf(); 
								
								?></div>
								<?php  	
							 }
	  							
	  							  ?>	
	  					<?php 
									 // echo $itemloop;
									 // echo " / ";
									
									// the_content('Mehr');
									
									// Variations for
									// PRESSE : 
									// we want the PDFs!
									
									// kk_get_pdf()
	  					?>
	  					
	  					</div>
	  					
	  					<?php edit_post_link('bearbeiten', '<p class="edit">[ ', ' ]</p>'); ?>
	  					
  					</div><!-- .list-item -->
  					
  				<?php 
  				endwhile; 
  				 // wp_reset_postdata();
  				 
  				 // reset some variables
  				 
  				 $kk_artist_name = '';
  				 
  				?>
  				
  				</div><!-- .list-item-box -->
  			</div><!-- .list-events-main-box -->
  				
  				<?php // Prevent weirdness
  				// wp_reset_postdata();
  				else : ?>
  				<?php 
  				endif; //connected MATERIALS
  				
  				// everything following this gets executed for every LOOP 1.
  				
  				?>
  				
  			<?php 
  			endwhile; 
  				// end of LOOP Nr 1.
  			?>
  				
  	</div> <!-- .superwide -->
  	
  	</article>
  
</div> <!-- #main -->


<?php get_footer(); ?>
